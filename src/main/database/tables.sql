create database library;

use library;

create table users (
username varchar(255) not null, 
password varchar(255) not null, 
email varchar(50) not null,
phone integer(50) not null,
enabled smallint(2) not null);

 create table 
 category (
 id Integer(50) unsigned not null auto_increment, 
 name varchar(255) not null, 
 status varchar(20) not null,
 primary key(id) );
 
 alter table category add column parent_id integer(50); 
 
 create table 
 basket (
 id Integer(50) unsigned not null auto_increment, 
 reader_id integer(50) unsigned, 
 foreign key(reader_id) references readers(id) on delete no action, 
 book_id integer(50) unsigned, 
 foreign key(book_id) references books(id) on delete no action, 
 primary key(id) );

 alter table basket add column nomer integer(50); 
  alter table basket add column session_id varchar(100);
 
 create table authors (
 id integer(50) unsigned not null auto_increment, 
 surname varchar(255) not null, 
 name varchar(255) not null, 
 nationality varchar(50) not null, 
 birthdate date not null, 
 primary key(id) );

 create table readers ( 
 id integer(50) unsigned not null auto_increment, 
 surname varchar(255) not null, 
 name varchar(255) not null, 
 register varchar(10) not null, 
 phone varchar(50) not null, 
 email varchar(255) not null, 
 address varchar(255) not null, 
 status varchar(20) not null, 
 primary key(id) );
 
alter table readers drop column password;
 
 create table image (
	id integer unsigned auto_increment,
	path varchar(255) not null,	
	description varchar(255),
	width integer unsigned,
	height integer unsigned,
	size integer unsigned,
	created datetime,
	primary key(id),
	key (created)
);

create table raw_file (
	id integer unsigned auto_increment,
	path varchar(255) not null,	
	description varchar(255),
	size integer unsigned,
	created datetime,
	primary key(id)
);

 create table books ( 
 id integer(50) unsigned not null auto_increment, 
 name varchar(255) not null, 
 barcode varchar(50) not null,
 category_id integer(50) unsigned not null, 
 foreign key(category_id) references category(id) on delete no action, 
 author_id integer(50) unsigned not null, 
 foreign key(author_id) references authors(id) on delete no action, 
 username varchar(50) not null, 
 foreign key(username) references users(username) on delete no action, 
 image_id integer unsigned not null,
 foreign key (image_id) references image(id),
 status varchar(20) not null, 
 printeddate date not null, 
 createddate date not null DEFAULT (CURRENT_DATE),
 introduction text,
 
 primary key(id));
 
 
  create table basket ( 
 id integer(50) unsigned not null auto_increment, 
 token varchar(255) not null, 
 enterdate date not null DEFAULT (CURRENT_DATE),
 status varchar(50),
 primary key(id));

 
 create table basket_item ( 
 id integer(50) unsigned not null auto_increment, 
 basket_id integer(50) unsigned not null,
 foreign key(basket_id) references basket(id) on delete no action,
 book_id integer(50) unsigned not null,
 foreign key(book_id) references books(id) on delete no action,
 enterdate date not null DEFAULT (CURRENT_DATE),
status varchar(55),
 primary key(id));
 
 
create table adminbasket ( 
id integer(50) unsigned not null auto_increment, 
 reader_id integer(50) unsigned not null, 
foreign key(reader_id) references readers(id) on delete no action, 
book_id integer(50) unsigned not null, 
foreign key(book_id) references books(id) on delete no action,
primary key(id));

 
create table reader_has_book (
id integer(50) unsigned not null auto_increment, 
book_id integer(50) unsigned not null, 
foreign key(book_id) references books(id) on delete no action, 
reader_id integer(50) unsigned not null, 
foreign key(reader_id) references readers(id) on delete no action, 
username varchar(50) not null, 
foreign key(username) references users(username) on delete no action, 
givendate datetime not null, 
expireddate datetime not null, 
status varchar(20) not null, 
primary key(id));