<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row justify-content-center">
	<div class="col-8">
				
		<div class="d-flex justify-content-between align-items-center">
			<h1>Хэрэглэгч</h1>
			<!-- <button class="btn btn-primary" onclick="create();">Шинэ</button> -->
			
			<button class="button button2" onclick="create();">
				Шинээр нэмэх<i class="fas fa-notes-medical" style="margin-left: 5px"></i>
			</button>
		</div>

		<div id="list"></div>
		
	</div>
</div>


<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" id="edit"></div>

</div>

<script>
	 /* var loadList = function() {
		$.get("/admin/user/list", function(data) {
			$("#list").html(data);
		});
	}
	loadList();  */
	
	var loadList = function(page, name) {
		$.get("/admin/user/list?page=" + page + "&size=5&username=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");  
	
	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/user/new", function(data) {
			$("#edit").html(data);
		});

	}
	
	var showRole = function(username) {
		$('#editModal').modal('show');
		$.get("/admin/authority/list?username=" + username, function(data) {
			$("#edit").html(data);
		})
	}

	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/user/" + id + "/useredit", function(data) {
			$("#edit").html(data);
		})
	}

	var submitForm = function() {
		var pass1 = document.getElementById("password").value.trim();
		var pass2 = document.getElementById("confirmPassword").value.trim();
		

		if (pass1 != pass2) { 
	        alert ("\n Нууц үгийн баталгаажуулалт зөрсөн: Дахин баталгаажуулна уу") 
	        return; 
	    } 
		
		if(document.getElementById("username").value.trim() == '') { 
	 		alert("Нэвтрэх нэр оруулна уу");
	 		$("#username").focus();
	 		return;
	 	}
		
		if(document.getElementById("password").value.trim() == '') { 
	 		alert("Нууц үг оруулна уу");
	 		$("#password").focus();
	 		return;
	 	}
		
		
		if(document.getElementById("email").value.trim() == '') { 
	 		alert("email оруулна уу");
	 		$("#email").focus();
	 		return;
	 	}
		
		if(document.getElementById("phone").value.trim() == '') { 
	 		alert("Гар утасны дугаар оруулна уу");
	 		$("#phone").focus();
	 		return;
	 	}
		
		
		
		$.post("/admin/user/save", $('#editForm').serialize(), function() {
			$('#editModal').modal('hide');
			$("#edit").html('');
			loadList(0, "");
		});
	}
	

</script>