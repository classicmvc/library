<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Хэрэглэгч нэмэх/засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editForm">
			<%-- <form:hidden path="id" /> --%>

			<div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<form:label path="username" class="col-md-1">Нэр</form:label>
				<div class="col-md-10">
					<form:input path="username" class="form-control" maxlength="20" placeholder="20 тэмдэгтээс хэтрэхгүй"/>
				</div>
			</div>


		 <div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<form:label path="password">Нууц үг</form:label>
				<div class="col-md-10">
					<form:input type="password" path="password" class="form-control" name="password1" maxlength="20" placeholder="20 тэмдэгтээс хэтрэхгүй"/>
				</div>
			</div>
			 <div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<form:label path="confirmPassword">Баталгаажуулах нууц үг</form:label>
				<div class="col-md-10">
					<form:input type="password" path="confirmPassword" class="form-control" name="password2" maxlength="20" placeholder="Давтан баталгаажуул"/>
				</div>
			</div>  
			
			<div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<form:label path="email">Имейл</form:label>
				<div class="col-md-10">
					<form:input path="email" class="form-control" maxlength="30" placeholder="30 тэмдэгтээс хэтрэхгүй"/>
				</div>
			</div>
			<div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<form:label path="phone">Гар утас</form:label>
				<div class="col-md-10">
					<form:input path="phone" class="form-control" maxlength="20" placeholder="20 тэмдэгтээс хэтрэхгүй"/>
				</div>
			</div>
			<div class="form-group row">

				<p style="color: red; margin-left: 10px">*</p>
				<form:radiobutton path="enabled" value="1" label="Идэвхтэй"
					checked="true" />
				<form:radiobutton path="enabled" value="0" label="Идэвхгүй" />
			</div>

			<div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<label>Хандалтын эрх сонгоно уу</label> 
				<div class="table-responsive">
				<script>
					function checkAll(action) {
						
						if($('#check-' + action).prop("checked")){
							<c:forEach var="row" items="${roles}">
							$('#ROLE_${row[0]}_' + action).prop("checked", true);
							</c:forEach>						
						}
						else {
							<c:forEach var="row" items="${roles}">
							$('#ROLE_${row[0]}_'  + action).prop("checked", false);
							</c:forEach>
						}
						
						
					}
				
				
					
				</script>
					<table class="table table-bordered" >
							<thead>
								<tr>
									<th></th>
									<th><input type="checkbox" id="check-EDIT" onclick="checkAll('EDIT');" />Edit</th>
									<th><input type="checkbox" id="check-DELETE" onclick="checkAll('DELETE');" />Delete</th>
									<th><input type="checkbox" id="check-SAVE" onclick="checkAll('SAVE');"/>Save</th>
									<th><input type="checkbox" id="check-UPDATE" onclick="checkAll('UPDATE');"/>Update</th>
									<th><input type="checkbox" id="check-LIST" onclick="checkAll('LIST');"/>List</th>
									<th><input type="checkbox" id="check-MAIN" onclick="checkAll('MAIN');"/>Main</th>
									<th><input type="checkbox" id="check-INDEX" onclick="checkAll('INDEX');"/>Index</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach var="row" items="${roles}">
								<tr>
								 <th scope="row">${row[0]}</th>
									<c:forEach var="role" items="${row}" varStatus="status">
										<c:if test="${status.index ne 0}">
										<td>
										<label> 
												<c:set var="contains" value="false" />
										
												<c:forEach var="item" items="${jspform.authorities}">
													<c:if test="${item eq role}">
														<c:set var="contains" value="true" />
													</c:if>
												</c:forEach> 
												
												<c:choose>
													<c:when test="${contains}">
													<input name="authorities" type="checkbox"  id="${role}" value="${role}" checked="checked"/>
													</c:when>
													<c:otherwise>
													<input name="authorities" type="checkbox" id="${role}" value="${role}" />
													</c:otherwise>
												</c:choose>
												<%-- ${role} --%>
											</label>
										</td>
										</c:if>
										
									</c:forEach>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

			</div>

		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
	</div>
</div>


