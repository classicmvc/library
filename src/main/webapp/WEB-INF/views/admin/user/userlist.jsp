<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${empty users}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Номын санч</th>
					<!-- <th>Нууц үг</th> -->
					<th>И-мейл</th>					
					<th>Гар утас</th>	
					<th>Төлөв</th>					
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${users.content}" var="user">
					<tr>			
						<td>${user.username}</td>
						<%-- <td>${user.password}</td>		 --%>			
						<td>${user.email}</td>	
						<td>${user.phone}</td>
						<td>${user.enabled}</td>					
						<td style="white-space: nowrap;">
							<button class="button button4" onclick="editThis('${user.username}')">
								Засах<i class="far fa-file-alt" style="margin-left: 5px"></i>
							</button>
							<button class="button button3" onclick="deleteThis('${user.username}')">
								Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
							</button>
							</td>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
	<c:if test="${not (users.first && users.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not users.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${users.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${users.totalPages}">
						<li
							class="page-item <c:if test="${users.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not users.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${users.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if> 
		


	</c:otherwise>
</c:choose>


<div class="modal fade" id="deleteModal" role="dialog">
	<div class="modal-dialog">

		Modal content
		<div class="modal-content">
			<div class="modal-body">
				<p>Энэ номын санч <span id="countAuthorities">0</span> эрхтэй.</p>
				<p>Энэ номын санч <span id="countBook">0</span> ном бүртгэсэн.</p>
				<p>Энэ номын санч <span id="countRHBook">0</span> үйлчилгээ үзүүлсэн.</p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary" id="showLink">Дэлгэрэнгүй харах</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Хаах</button>
			</div>
		</div>

	</div>
</div>



<script>

 var paginate = function (page) {
	loadList(page, $('#filterName').val());
}

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/user/" + id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function(message) {
					  if (message=="Fail") {
						  $.get('/admin/user/'+id+'/count', function(count){
							//  alert(count + "ном ашиглагдсан байна");
							$('#countAuthorities').text(count);
							$('#countBook').text(count);
							$('#countRHBook').text(count);
							$('#deleteLink').attr('href', '/admin/book?username=' + id);
							  $("#myModal").modal();
						  });
						  
					  }
					  else {
						  alert("Амжиллтай устлаа");
						  loadList(0, "");
					  }			  
					  			  	   
				  }
			});	
		}				
	}
	
	
	/*var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/author/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function(message) {
					  if (message=="Fail") {
						  $.get('/admin/author/'+id+'/count', function(count){
							//  alert(count + "ном ашиглагдсан байна");
							$('#deleteCount').text(count);
							$('#deleteLink').attr('href', '/admin/book?authorId=' + id);
							  $("#myModal").modal();
						  });
						  
					  }
					  else {
						  alert("Амжиллтай устлаа");
						  loadList(0, "");
					  }			  
					  			  	   
				  }
			});	
		}				
	}*/
	
</script>

