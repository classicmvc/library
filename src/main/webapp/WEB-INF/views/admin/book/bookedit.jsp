<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">
	<div class="d-flex justify-content-between align-items-center">
		<h1>Ном нэмэх, засах</h1>
		<!--  <a href="/admin/book" class="button back">Буцах<i class="fas fa-undo-alt" style="margin-left: 5px"></i></a> -->

	</div>

	<form:form modelAttribute="jspform" id="editForm"
		action="/admin/book/save">
		<form:hidden path="id" />

		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label path="name" class="col-md-1">Номын нэр</form:label>
			<div class="col-md-10">
				<form:input path="name" maxlength="50" cssClass="form-control"
					cssErrorClass="form-control is-invalid"
					placeholder="50 тэмдэгтээс хэтрэхгүй" />
				<form:errors path="name" cssClass="invalid-feedback" element="div" />
			</div>
		</div>

		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label path="barcode" class="col-md-1">Бар код</form:label>
			<div class="col-md-10">
				<form:input type="number" pattern="/^-?\d+\.?\d*$/"
					onKeyPress="if(this.value.length==20) return false;" path="barcode"
					cssClass="form-control" cssErrorClass="form-control is-invalid"
					placeholder="20 тэмдэгтээс хэтрэхгүй" />
				<form:errors path="barcode" cssClass="invalid-feedback"
					element="div" />
			</div>
		</div>

		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label path="categoryId" class="col-md-1">Ангилал сонгох</form:label>
			<div class="col-md-10" onclick="loadCategories();">
				<form:hidden path="categoryId"/>
				<form:input path="categoryName" class="form-control" />	
				<form:errors path="categoryId" cssClass="invalid-feedback"
					element="div" />
			</div>
		</div>

		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label path="authorId" class="col-md-1">Зохиолч сонгох</form:label>
			<div class="col-md-10" onclick="loadAuthors();">
				<form:hidden path="authorId" />
				<form:input path="authorName" cssClass="form-control"
					cssErrorClass="form-control is-invalid" />
				<form:errors path="authorId" cssClass="invalid-feedback"
					element="div" />
			</div>
		</div>

		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label path="imageId" class="col-md-1">Зураг сонгох</form:label>
			<div class="col-md-10" onclick="loadImages(0);">
				<form:hidden path="imageId" />
				<form:input path="description" cssClass="form-control"
					cssErrorClass="form-control is-invalid" />
				<form:errors path="imageId" cssClass="invalid-feedback"
					element="div" />
			</div>
		</div>


		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label class="col-md-1" path="status">Төлөв сонго</form:label>
			<div class="col-md-10">
				<form:select path="status" style="width:200px;">
					<form:option value="Идэвхтэй">Идэвхтэй</form:option>
					<form:option value="Идэвхгүй">Идэвхгүй</form:option>
				</form:select>
			</div>
		</div>
		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label class="col-md-1" path="printeddate">Хэвлэсэн огноо</form:label>
			<div class="col-md-10">
				<form:input type="date" id="printeddate" max="" path="printeddate"
					class="form-control" />
			</div>
		</div>

		<div class="form-group row">
			<p style="color: red; margin-left: 10px">*</p>
			<form:label class="col-md-1" path="introduction">Танилцуулга</form:label>
			<div class="col-md-10">
				<form:textarea maxlength="300" path="introduction"
					cssClass="form-control" cssErrorClass="form-control is-invalid"
					placeholder="300 тэмдэгтээс хэтрэхгүй" />
				<form:errors path="introduction" cssClass="invalid-feedback"
					element="div" />
			</div>
		</div>

		<div class="d-flex bd-highlight mb-2">
				<div class="mr-auto p-2 bd-highlight">
					<a href="/admin/book" class="btn btn-secondary">Буцах</a>
				</div>
				<div class="p-2 bd-highlight">
					<button class="button save" onclick="$('#editForm').submit();">
						Хадгалах<i class="fas fa-save" style="margin-left: 5px"></i>
					</button>
				</div>
			</div>


	</form:form>
</div>
<style>
.customModal {
	position: fixed;
	top: 0;
	right: 0;
	left: 0;
	bottom: 0;
	background: rgba(255, 255, 255, 0.8);
	display: none;
	z-index: 1050;
}

.customModal--open {
	display: block;
}
</style>

<div class="customModal" id="customModal">
	<div class="card" id="modal"></div>
</div>


<script>

var loadCategories = function(page) {
	$('#customModal').addClass("customModal--open");
		$.get("/admin/category/main?select=1", function(data) {
			$("#modal").html(data);
		});
	}
	
var selectCategory = function(id, name) {
	$("#categoryId").val(id);
	$("#categoryName").val(name);
	$.get("/admin/category/categoryselected/"+id, function(data) {
		$("#category").html(data);
	});		
	$("#modal").html("");
	$('#customModal').removeClass("customModal--open");
}

 var loadAuthors = function(page) {
	$('#customModal').addClass("customModal--open");
		$.get("/admin/author/main?select=1", function(data) {
			$("#modal").html(data);
		});
 }

var selectAuthor = function(id, name) {
		$("#authorId").val(id);
		$("#authorName").val(name);
		$.get("/admin/author/authorselected/"+id, function(data) {
			$("#author").html(data);
		});		
		$("#modal").html("");
		$('#customModal').removeClass("customModal--open");
	}
	
	/* <c:if test="${!empty jspform.authorId}">
		selectAuthor(${jspform.authorId});
	</c:if>	 */
	
	var loadImages = function(page) {
		$('#customModal').addClass("customModal--open");
			$.get("/admin/image/main?select=1", function(data) {
				$("#modal").html(data);
		});
		
	}
	
	var selectImage = function(id,name) {
		$("#imageId").val(id);
		$("#description").val(name);
		$.get("/admin/image/imageselected/"+id, function(data) {
			$("#image").html(data);
		});		
		$("#modal").html("");
		$('#customModal').removeClass("customModal--open");
	}
	
	
	/* <c:if test="${!empty jspform.imageId}">
		selectImage(${jspform.imageId});
	</c:if>	 */
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	 if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 

	today = yyyy+'-'+mm+'-'+dd;
	document.getElementById("printeddate").setAttribute("max", today);
	
</script>

