<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="table-responsive">
	<table class="table table-bordered">
		<tbody>
			<c:forEach items="${books}" var="book">
				<tr> 
					<td onclick="addThis(${book.id})" style="cursor: pointer; user-select: none;">${book.name}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>


