<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${books.numberOfElements eq 0}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th style="width: 1px;">id</th> -->
					<th>Зураг</th>
					<th>Номын нэр</th>
					<th>Баар код</th>
					<th>Категори</th>
					<th>Зохиолч</th>
					<th>Хэн</th>
					<th>Төлөв</th>
					<th>Хэвлэсэн огноо</th>
					<th>Хэзээ</th>
					<th>Танилцуулга</th>
					<th style="width: 1px;">Үйлдэл</th>
				</tr>

			</thead>
			<tbody>
				<c:forEach items="${books.content}" var="book">
					<tr>
						<%-- <td>${book.id}</td>  --%>
						<td><c:if test="${!empty book.image}">
								<img src="/${book.image.path}"
									class="img-thumbnail rounded d-block mr-2 mb-2"
									style="max-width: auto; height: 100px;" />
							</c:if></td>
						<td>${book.name}</td>
						<td>${book.barcode}</td>
						<td>${book.category.name}</td>
						<td>${book.author.name}</td>
						<td>${book.username}</td>
						<td>${book.status}</td>
						<td>${book.printeddate}</td>
						<td>${book.createddate}</td>
						<td>${book.introduction} ${param.readerId}</td>
						<td style="white-space: nowrap;">	
						
						<a class="button button4"
							href="/admin/book/${book.id}/bookedit">Засах<i
								class="far fa-file-alt" style="margin-left: 5px"></i>
						</a>
							<button class="button button3" type="button"
								onclick="deleteThis(${book.id})">
								Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
							</button>


						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<c:if test="${not (books.first && books.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not books.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${books.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${books.totalPages}">
						<li
							class="page-item <c:if test="${books.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not books.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${books.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>

	</c:otherwise>
</c:choose>

<script>

	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/book/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList(0, "");			  	   
				  }
			});	
		}				
	}
</script>