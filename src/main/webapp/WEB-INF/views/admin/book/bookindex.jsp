<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center">
	<h1>Ном</h1>
	<a href="/admin/book/new" class="button button2">Шинээр нэмэх<i
		class="fas fa-notes-medical" style="margin-left: 5px"></i></a>
	<!-- <button class="btn btn-primary" onclick="create();">Шинэ</button>-->
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
		type="text" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Хайлтын утгаа оруулна уу" onkeypress="handle(event)" maxlength="20">
	<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
</form>

<div id="list"></div>


<script>
function handle(e){
    if(e.keyCode === 13){
        e.preventDefault(); // Ensure it is only this code that rusn
         searchList(0, "");
    }
} 


	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, name) {
		$.get("/admin/book/list?page=" + page + "&size=10&authorId=${param.authorId}&name=" + name,
				function(data) {
					$("#list").html(data);
				});
	}
	
	
	
	loadList(0, "");
</script>