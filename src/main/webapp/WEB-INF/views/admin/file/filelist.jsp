<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${empty files}">
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width: 1px;">id</th>
					<th>Зам</th>
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${files}" var="file">
					<tr>
						<td>${file.id}</td>
						<td>${file.path}</td>
						<td style="white-space: nowrap;">							
							<a href="/${file.path}" class="btn btn-dark" download>Татах</a>						
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>


	</c:otherwise>
</c:choose>