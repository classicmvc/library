<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row justify-content-center">
	<div class="col-8">

		<div class="d-flex justify-content-between align-items-center">
			<h1>Файл</h1>
		</div>

		<form method="POST" enctype="multipart/form-data"
			action="/admin/file/upload">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" /> <input type="file" name="file" /> <input
				type="submit" value="Upload" />

		</form>
		
		<div id="list"></div>

	</div>
</div>

<script>
	var loadList = function() {
		$.get("/admin/file/list?sort=created,desc", function(data) {
			$("#list").html(data);
		});
	}
	loadList();	
</script>