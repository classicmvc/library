<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Зохиогч нэмэх/засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editauthorForm">
			<form:hidden path="id" />
			
			<div class="form-group">
				<form:label path="surname">Овог</form:label>
				<form:input path="surname" class="form-control" maxlength="20" placeholder="20 тэмдэгтээс хэтрэхгүй"/>
			</div>
			<div class="form-group">
				<form:label path="name">Нэр</form:label>
				<form:input path="name" class="form-control" maxlength="20" placeholder="20 тэмдэгтээс хэтрэхгүй"/>
			</div>
			<div class="form-group">
				<form:label path="nationality">Иргэншлээ сонгоно уу</form:label>
				<form:select path="nationality">
					<form:option value="Монгол">Монгол</form:option>
					<form:option value="Орос">Орос</form:option>
					<form:option value="Хятад">Хятад</form:option>
					<form:option value="Англи">Англи</form:option>
					<form:option value="АНУ">АНУ</form:option>
					<form:option value="Казакстан">Казакстан</form:option>
					<form:option value="Япон">Япон</form:option>
					<form:option value="БНСУ">БНСУ</form:option>
				</form:select>
			</div>
			<div class="form-group">
				<form:label path="birthdate">Төрсөн огноо</form:label>
				<form:input path="birthdate" id="birthdate" class="form-control" type="date" max="" maxlength="10" placeholder="20 тэмдэгтээс хэтрэхгүй"/>
			</div>
			
		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>

<script>

var selectAuthor = function(id) {
	$("#authorId").val(id);
	$.get("/admin/author/select/"+id, function(data) {
		$("#author").html(data);
	});		
	$("#authors").html("");
}

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("birthdate").setAttribute("max", today);
</script>
