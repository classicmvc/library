<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row justify-content-center">
	<div class="col-8">
		<div class="d-flex justify-content-between align-items-center">
			<h1>Зохиолч</h1>
			<button class="button button2" onclick="create();">
				Шинээр нэмэх<i class="fas fa-notes-medical" style="margin-left: 5px"></i>
			</button>
		</div>
		
		<form class="form-inline">
			<label class="sr-only" for="inlineFormInputName2">Нэр</label> 
			<input type="text" class="form-control mb-2 mr-sm-2" id="filterName"
				placeholder="Хайлтын утгаа оруулна уу" onkeypress="handle(event)" maxlength="20">
			<button type="button" class="btn btn-dark mb-2" onclick="searchList();" >Хайх</button>
		</form>
		
		<div id="list"></div>
	</div>
</div>
		
		<div id="editModal" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" id="edit"></div>
		</div>

<script>


function handle(e){
    if(e.keyCode === 13){
        e.preventDefault(); // Ensure it is only this code that rusn
         searchList(0, "");
    }
} 

	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, name) {
		$.get("/admin/author/list?page=" + page
				+ "&size=10&name=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");

	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/author/new", function(data) {
			$("#edit").html(data);
		});

	}
	
	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/author/" + id + "/authoredit", function(data) {
			$("#edit").html(data);
		})
	}

	var submitForm = function() {
		
		if(document.getElementById("surname").value.trim() == '') { 
	 		alert("Овог оруулна уу");
	 		$("#surname").focus();
	 		return;
	 	}
		
		if(document.getElementById("name").value.trim() == '') { 
	 		alert("Нэр оруулна уу");
	 		$("#name").focus();
	 		return;
	 	}
						
		$.post("/admin/author/save", $('#editauthorForm').serialize(), function(data) {
			if (data==='duplicate'){
				alert('Энэ зохиолч аль хэдийн сонгогдсон');	
			}
			else if (data==='success'){
			$('#editModal').modal('hide');
			$("#edit").html('');
			loadList(0, "");
			}
		});
	}
	
	
</script>