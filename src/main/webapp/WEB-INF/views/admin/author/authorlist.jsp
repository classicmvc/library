<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<c:choose>
	<c:when test="${authors.numberOfElements eq 0}">
		
		<table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th style="width: 1px;">№</th> -->
					<th>Овог</th>
					<th>Нэр</th>
					<th>Иргэншил</th>
					<th>Төрсөн огноо</th>
					<th style="width: 1px;"></th>
				</tr>
			</thead>
		</table>
				<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th style="width: 1px;">№</th> -->
					<th>Овог</th>
					<th>Нэр</th>
					<th>Иргэншил</th>
					<th>Төрсөн огноо</th>
					<th style="width: 1px;"></th>
				</tr>

			</thead>
			<tbody>
				<c:forEach items="${authors.content}" var="author">
					<tr>
						<%-- <td>${author.id}</td> --%>
						<td>${author.surname}</td>
						<td>${author.name}</td>
						<td>${author.nationality}</td>
						<td><fmt:formatDate value="${author.birthdate}"
								pattern="yyyy-MM-dd" /></td>
						<td style="white-space: nowrap;"><c:if
								test="${param.select eq 1}">
								<button class="button button1"
									onclick="selectAuthor(${author.id},'${author.name}')">
									Сонгох<i class="fas fa-check-circle" style="margin-left: 5px"></i>
								</button>

							</c:if>
							<button class="button button4" onclick="editThis(${author.id})">
								Засах<i class="far fa-file-alt" style="margin-left: 5px"></i>
							</button>
							<button class="button button3" onclick="deleteThis(${author.id})">
								Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
							</button></td>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<c:if test="${not (authors.first && authors.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not authors.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${authors.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${authors.totalPages}">
						<li
							class="page-item <c:if test="${authors.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not authors.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${authors.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>

	</c:otherwise>
</c:choose>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<p>
					Энэ зохиолч <span id="deleteCount">0</span> ном дээр бүртгэлтэй
					байна. 
				</p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary" id="deleteLink">Дэлгэрэнгүй харах</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Хаах</button>
			</div>
		</div>

	</div>
</div>
<!-- End Modal -->

<script>

var authorBookList = function(page, name) {
	$.get("/admin/author/list?page=" + page + "&size=10",
			function(data) {
				$("#authorbooklist").html(data);
			});
}
authorBookList(0, "");


	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/author/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function(message) {
					  if (message=="Fail") {
						  $.get('/admin/author/'+id+'/count', function(count){
							//  alert(count + "ном ашиглагдсан байна");
							$('#deleteCount').text(count);
							$('#deleteLink').attr('href', '/admin/book?authorId=' + id);
							  $("#myModal").modal();
						  });
						  
					  }
					  else {
						  alert("Амжиллтай устлаа");
						  loadList(0, "");
					  }			  
					  			  	   
				  }
			});	
		}				
	}
</script>







