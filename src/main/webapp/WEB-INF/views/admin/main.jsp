
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!doctype html>
<html lang="en">
<head>
<title>Номын сангийн админ хэсэг</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<style type="text/css">
.body {
	margin: 0;
	font-family: "Lato", sans-serif;
	background-color: #F8F8F8;
}

.inline__block {
	display: inline-block;
}

header {
	display: flex;
	justify-content: space-between;
	padding: 10px 5px;
	background-color: #e6e6e6;
}

.menu {
	overflow: hidden;
}

header {
	background-color: black;
	color: white;
	position: fixed;
	width: 100%;
	z-index: 100;
}

h1{
  color: black;
  text-shadow: 2px 2px 4px #000000;
  margin-bottom:40px;
}

.button1 {
  background-color: white;
  color: #39ac39;
  border: 2px solid #39ac39;
  border-radius: 4px;
}

.button1:hover {
  background-color: #39ac39; 
  color: white;
}


.button2 {
  background-color: white; 
  color: #008CBA; 
  border: 2px solid #008CBA;
  border-radius: 4px;
}

.button2:hover {
  background-color: #008CBA;
  color: white;
}
		.button3 {
  background-color: white; 
  color: red; 
  border: 2px solid #f44336;
  border-radius: 4px;
}

.button3:hover {
  background-color: #f44336;
  color: white;
}

.button4 {
  background-color: white;
  color: #A8A8A8;
  border: 2px solid #A8A8A8;
  border-radius: 4px;
}

.button6 {
  background-color: #009900;
  color: white ;
  border: 2px solid #009900;
  border-radius: 4px;
}

.button4:hover {
	background-color: #C8C8C8; 
	color: white;
}
.file {
  visibility: hidden;
  position: absolute;
}

.center {
  text-align: center;
}

nav {
  display: inline-block;
}
nav a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #5c85d6;
  margin: 0 4px;
}
nav a:hover:not(.active) {background-color: #d6e0f5;}

.sidebar {
	width: 200px;
	background-color: #323336;
	position: fixed;
	height: 100%;
	overflow: auto;
	left: 0;
	transition: left 0.5s;
	margin-top: 40px;
}

.sidebar a {
	display: block;
	color: black;
	padding: 16px;
	text-decoration: none;
}

.sidebar a.active {
	background-color: #323336;
	color: white;
}

.sidebar a:hover 
:not(
.active)
{background-color:#555;color:white;}
.sidebar-open {
	left: -200px;
}

.sidebar2 {
	width: 200px;
	background-color: #323336;
	position: fixed;
	height: 100%;
	overflow: auto;
	transition: left 0.5s;
	margin-top: 40px;
	width: 50px;
}

.sidebar2 a {
	display: block;
	color: black;
	padding: 16px;
	text-decoration: none;
}

.sidebar2 a.active {background-color: #323336; color: white;}
.sidebar2
a:hover
:not
(.active)
{background-color: #555;
color: white;}
.sidebar2-open {
	left: -200px;
}
.content {
	margin-left: 200px;
	padding: 70px 16px;
}
@media screen and (max-width: 700px) {
	.sidebar {
		width: 100%;
		height: auto;
		position: relative;
}
	.sidebar a {
		float: left;
	}
}

@media screen and (max-width: 400px) {
	.sidebar a {
		text-align: center;
		float: none;
	}
}

i {
	margin-left: 5px;
	margin-right: 20px;
}

button {
	background-color: black;
	color: white;
	border: black;
}

.btn {
	color: white;
}

.btn-group {
	margin-right: 0;
	color: white;
}

.dropdown-menu {
	background-color: black;
}

.menu {
	background-color: white;
	height: 140px;
}


</style>
</head>
<body>
	<header>
		<div class="left">
			<div class="inline__block">
				<i class="fas fa-align-justify active" id="slider__menu"
					onclick="toggleSidebar()"></i>
			</div>
			<div class="inline__block">Номын сан</div>
		</div>
		<div class="right">
			<div class="btn-group">
				<!-- <button type="button" class="btn" data-toggle="modal"
					data-target="#exampleModal">
					<i class="far fa-comment"></i>
				</button> -->
				<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">...</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary">Save
									changes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="btn-group">
				 <button class="btn btn-default dropdown-toggle" type="button"
					data-toggle="dropdown">
					<i class="far fa-question-circle"></i> <span class="caret"></span>
				</button> 
				<ul class="dropdown-menu">
					<li class="dropdown-header">
						<button>
							<i class="far fa-question-circle"></i>Page Help
						</button>
					</li>
					<div class="dropdown-divider"></div>
					<li class="dropdown-header">
						<button>
							<i class="fas fa-exclamation-circle"></i>About Page
						</button>
					</li>
				</ul>
			</div> -->
			
			<sec:authentication var="user" property="principal" />
			
			<div class="btn-group">
				<button class="btn btn-default dropdown-toggle" type="button"
					data-toggle="dropdown">
					 ${user.username}<span class="caret"></span> 
				</button>
				<ul class="dropdown-menu">		
					<li class="dropdown-header">
							<form action="/logout" method="post">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button><i class="fas fa-sign-out-alt"></i>Гарах</button>
							</form>
					</li>
					
				</ul>
			</div>
		</div>
	</header>
	<div class="sidebar">
		 <%@include file="menuwithtext.jsp"%>
	</div>
	<div class="sidebar2">
			 <%@include file="menuwithouttext.jsp"%>
	</div>

 	 <div class="content">
		<div class="library">
			<tiles:insertAttribute name="body" />	
		</div>
	</div>  
	
	<script type="text/javascript">
		var toggleSidebar = function() {
			$('.sidebar').toggleClass("sidebar-open");
		}
		$(document).ready(function() {
			$("#btn").click(function() {
				alert("Text: " + $("#test").text());
			});
		});
	</script>
</body>
</html>