<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
				<!-- <th>№</th> -->
				<th>Номын нэр</th>
				<th>Уншигч</th>
				<th>Ажилтан</th>
				<th>Бүртгэсэн огноо</th>
				<th>Дуусах огноо</th>
				<th>Төлөв</th>
				<th>Үйлдэл</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${rhbooks.content}" var="rhbook">
				<tr>
					<%-- <td>${rhbook.id}</td> --%>
					<td>${rhbook.book.name}</td>
					<td>${rhbook.reader.name}</td>
					<td>${rhbook.username}</td>
					<td>${rhbook.givendate}</td>
					<td>${rhbook.expireddate}</td>
					<td>${rhbook.status}</td>
					<td style="white-space: nowrap;"><a class="button button4"
						href="/admin/rhbook/${rhbook.id}/rturn/">Буцаах<i
							class="far fa-file-alt" style="margin-left: 5px"></i>
					</a>
						<button class="button button3" type="button"
							onclick="deleteThis(${book.id})">
							Сунгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
						</button></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<c:if test="${not (rhbooks.first && rhbooks.last)}">
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			<c:if test="${not rhbooks.first}">
				<li class="page-item"><a class="page-link" href="#"
					onclick="paginate(${rhbooks.number-1});">Өмнөх</a></li>
			</c:if>
			<c:forEach var="i" begin="1" end="${rhbooks.totalPages}">
				<li
					class="page-item <c:if test="${rhbooks.number == i-1}">active</c:if>">
					<a class="page-link" href="#" onclick="paginate(${i-1});"> ${i}
				</a>
				</li>
			</c:forEach>
			<c:if test="${not rhbooks.last}">
				<li class="page-item"><a class="page-link" href="#"
					onclick="paginate(${rhbooks.number+1});">Дараах</a></li>
			</c:if>
		</ul>
	</nav>
</c:if>



<%-- <div id="idtest">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Захиалсан ном</th>
				<th>Токен</th>
				<th>Огноо</th>
				<th>Төлөв</th>
				<th style="width: 1px;">Үйлдэл</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${basketitems}" var="basketitem" varStatus="status">
				<tr>
					<td>${basketitem.book.name}</td>
					<td>${basketitem.basket.token}</td>
					<td>${basketitem.enterdate}</td>
					<td>${basketitem.status}</td>
					<td style="white-space: nowrap;">
						<button class="button button3" type="button"
							onclick="deleteThis(${basketitem.id})">
							Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
						</button>
					</td>
					<td id="total" style="display: none;">${status.index+1}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</div> --%>
