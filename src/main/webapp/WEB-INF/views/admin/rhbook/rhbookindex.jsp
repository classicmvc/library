<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="d-flex justify-content-between align-items-center">
	<h1>Уншигчдын авсан номнууд</h1>
	<a href="/admin/reader?select=1" class="button button2">Шинээр нэмэх<i
		class="fas fa-notes-medical" style="margin-left: 5px"></i></a>
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Уншигч</label> <input
		type="text" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Уншигч ба номоор">
	<button type="button" class="btn bsearch" onclick="searchList()">
					Хайх <i class="fas fa-search"></i>
				</button>
</form>
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
        Түгээгдсэн номнууд   
	</a>
    <a class="nav-item nav-link" id="idtest" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Сагс</a>
    <!-- <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Нөөц</a> -->
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	<div id="rhblist"></div>
	</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
    <div id="wbitem">  </div>
  
  </div>
 <!--  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>  -->
</div> 


<style>
.bsearch {
	border-color: #23a393;
	color: #23a393;
}

.bsearch:hover {
	color: black;
	border-color: black;
}
</style>

<script>
	var searchList = function() {
		listReaderHasBook(0,$('#filterName').val());
	}

	var listReaderHasBook = function(page,name) {
		$.get("/admin/rhbook/rhbooklist?readerId=${param.readerId}&page=" + page + "&size=10&q=" + name, function(data) {
			$("#rhblist").html(data);
		});
		
		$.get("/admin/rhbook/webbasketlist", function(data) {
			$("#wbitem").html(data);
		});
	}
	listReaderHasBook(0,"");
	
	
	
	 var deleteThis = function (id) {
			if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
				$.ajax({
					  url: "/admin/adminbasket/webbasket/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
					  type: 'DELETE',
					  success: function() {
						  alert("Амжиллтай устлаа");
						 // location.reload();
						  listReaderHasBook(0,"");
					  }
				});	
			}				
		}
	
</script>
