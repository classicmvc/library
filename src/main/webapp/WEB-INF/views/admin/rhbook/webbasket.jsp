<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div>
			<table class="table table-bordered">
							<thead>
								<tr>
									<th>Захиалсан ном</th>
									 <th>Токен</th>
									 <th>Огноо</th>
									 <th>Төлөв</th>
									 <th style="width: 1px;">Үйлдэл</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${basketitems}" var="basketitem"
									varStatus="status">
									<tr>
										<td>${basketitem.book.name}</td>
										<td>${basketitem.basket.token}</td>
										<td>${basketitem.enterdate}</td>
										<td>${basketitem.status}</td>
										<td style="white-space: nowrap;">
						       <a class="button	button1" style="width:40px;" href="/admin/reader?basketitemid=${basketitem.id}"> 
						       	 Сонгох
							 	 <i class="fas fa-check-circle" style="margin-left: 5px"></i>
								</a>		
											<button class="button button3" type="button"
												onclick="deleteThis(${basketitem.id})">
												Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
											</button>
										</td>
										<td id="total" style="display: none;">${status.index+1}</td>
									</tr>
								</c:forEach>
							</tbody>
		</table>

	</div>
