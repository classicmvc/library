<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


		
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="text-align:center">Сонгогдсон ном</th>
			
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${adminbaskets}" var="adminbasket">
					<tr>
						<td><i class="fas fa-minus-circle mr-4" style="color:red" onclick="deleteThis(${adminbasket.id})">
						</i>${adminbasket.book.name}</td>				
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>	
	



<script>
   var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/adminbasket/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  loadList(0, "");			  	   
				  }
			});	
		}				
	}
	
</script>
