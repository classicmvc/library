<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center">
	<h3 class="title" style="color: black;text-shadow: 2px 2px 4px #000000;
		  margin-bottom:40px;">Номын сонголт</h3>
	
	
	<div class="card" style="width: 18rem;">
		<div class="card-body">
			<p class="card-text">
				<strong><%-- Дугаар: ${param.readerId}<br/> --%>
				    Нэр: ${adminBasket.name} <br/>
				    Рег№:${adminBasket.register} <br/>У
				    тас: ${adminBasket.phone}</strong>
			</p>
		</div>
	</div>
</div>

<div class="row d-flex justify-content-between">
	<form class="form col-sm-4">

		<div class="input-group mb-4 mt-4">
			<input type="text" class="form-control" id="filterName"
				placeholder="Номын нэрээр хайх">
			<div class="input-group-append" id="button-addon4">
				<button type="button" class="btn bsearch" onclick="searchList()">
					Хайх <i class="fas fa-search"></i>
				</button>
			</div>
		</div>
		<div id="categorylist"></div>
	</form>

	<div class="col-sm-8 mt-4">
		<div id="chooselist"></div>
		<div class="d-flex justify-content-end">
			<button class="btn btn-danger" type="button" onclick="submit();">Баталгаажуулах</button>
		</div>
	</div>

</div>
<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<style>
.bsearch {
	border-color: #23a393;
	color: #23a393;
}

.bsearch:hover {
	color: black;
	border-color: black;
}

.title {
	display: block;
	text-align: justify;
	line-height: 0.8;
	text-transform: uppercase;
	letter-spacing: 0px;
	color: #60a9d9;
	font-weight: bold;
	text-shadow: 0 1px 2px rgba(black, .15);
}
</style>


<script>

	var searchList = function() {
		
		listBooksOfAllCategory($('#filterName').val());
	}

	var loadCategory = function() {
		$.get("/admin/category/categorylisttobasket", function(data) {
			$("#categorylist").html(data);
		});
	}
	loadCategory();

	var addThis = function(id) {
		$.get("/admin/adminbasket/save?readerId=${param.readerId}&bookId="+ id, function(data) {
			if (data==='duplicate'){
				alert('Энэ ном аль хэдийн сонгогдсон');	
			}
			else if (data==='success'){
				loadList();	
			}			 
		})
	}

	var loadList = function() {
		$.get("/admin/adminbasket/adminbasketlist?readerId=${param.readerId}", function(data) {
			$("#chooselist").html(data);
		});
	}
	loadList();
	

	var submit = function() {
		$.post("/admin/rhbook/save?readerId=${param.readerId}&${_csrf.parameterName}=${_csrf.token}",
						function(data) {
							window.location = "/admin/rhbook"
						});
	}
</script>



