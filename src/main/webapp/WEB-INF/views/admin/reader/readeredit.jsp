<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Уншигч Нэмэх/Засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editForm">
			<form:hidden path="id" />

			<div class="form-group">
				<form:label path="surname">Овог</form:label>
				<form:input path="surname" class="form-control" maxlength="20"
					placeholder="20 тэмдэгтээс хэтрэхгүй" />
			</div>
			<div class="form-group">
				<form:label path="name">Нэр</form:label>
				<form:input path="name" class="form-control" maxlength="20"
					placeholder="20 тэмдэгтээс хэтрэхгүй" />
			</div>
			<div class="form-group">
				<form:label path="register">Регистр</form:label>
				<form:input path="register" class="form-control" id="register" oninput="handleChange()" maxlength="10"
					placeholder="10 тэмдэгтээс хэтрэхгүй" />
			</div>
			<div class="form-group">
				<form:label path="phone">Гар утас</form:label>
				<form:input type="number" path="phone" class="form-control" pattern="/^-?\d+\.?\d*$/"
					onKeyPress="if(this.value.length==20) return false;"
					placeholder="20 тэмдэгтээс хэтрэхгүй" />
			</div>
			<div class="form-group">
				<form:label path="email">И-мейл</form:label>
				<form:input type="email" path="email" class="form-control"
					maxlength="30" placeholder="20 тэмдэгтээс хэтрэхгүй" />
			</div>
			<div class="form-group">
				<form:label path="address">Хаяг</form:label>
				<form:textarea path="address" class="form-control" maxlength="200"
					placeholder="200 тэмдэгтээс хэтрэхгүй" />
			</div>
			<%-- <div class="form-group">
				<form:label path="status">Төлвөө сонгоно уу</form:label>
				<form:select path="status">
					<form:option value="Идэвхтэй">Идэвхтэй</form:option>
					<form:option value="Идэвхгүй">Идэвхгүй</form:option>
				</form:select>
				</div>
				 --%>

			<div class="form-group row">
				<p style="color: red; margin-left: 10px">*</p>
				<form:label class="col-md-1" path="status">Төлөв сонго</form:label>
				<div class="col-md-10">
					<form:select path="status" style="width:200px;">
						<form:option value="Идэвхтэй">Идэвхтэй</form:option>
						<form:option value="Идэвхгүй">Идэвхгүй</form:option>
					</form:select>
				</div>
			</div>

		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>

<script type="text/javascript">

	function isMongol(char){
		switch (char){
			case 'Ө':
			case 'ө':
			case 'Ү':
			case 'ү':
				return true;
			default:
				return /[а-яА-ЯЁё]/.test(char);
		}
		
		return false;
	}

	function isOdd(n) {
   		return Math.abs(n % 2) == 1;
	}

	function handleChange(){
		
		var register = document.getElementById('register').value.toUpperCase().split("");
		
		for (var i=0; i<register.length; i++){

			switch(i){
				case 0: 				
				case 1:
					if (!isMongol(register[i])){
						register.splice(i,1);
					}
					break;
				case 2:					
				case 3:
					if (isNaN(register[i])){
						register.splice(i,1);	
					}
					break;
				case 4:
					if (register[i]>3) {
						register.splice(i,1);
					}
					if (isNaN(register[i])){
						register.splice(i,1);	
					}
					break;
				case 5:
					if ((register[i-1]==3) || (register[i-1]==1)) {
						if (register[i]>2) {
							register.splice(i,1);
						}						
					}

					if (register[i-1]==2) {
					  if	(register[i]==0) {
							register.splice(i,1);
						}
					}
					if (isNaN(register[i])){
						register.splice(i,1);	
					}
					break;
				case 6:
					if (register[i-2]==0 && register[i-1]==2) {
				    	if (register[i]>2)
				    		register.splice(i,1);
				    }
				    if (register[i-2]==2 && register[i-1]==2) {
				    	if (register[i]>2)
				    		register.splice(i,1);
				    }
				    else
				    {
				    	if (register[i]>3) {
							register.splice(i,1);
						}	
				    }
				    if (isNaN(register[i])){
						register.splice(i,1);	
					}
					break;
				case 7:
					if (register[i-1]==3){

 						if (!isOdd(register[4]) && (register[5]<=7)) {
 							if (register[i]>1) {
								register.splice(i,1);
							}				
 						}
 						if ((register[5]==4) || (register[5]==6)){
 							if (register[i]>0) {
								register.splice(i,1);
							}				
 						}

 						if (register[5]==8) {
 							if (register[i]>1) {
								register.splice(i,1);
							}				
 						}

 						if (register[5]==9) {
 							if (register[i]>0) {
								register.splice(i,1);
							}				
 						}

 						if (register[5]==0) {
 							if (register[i]>1) {
								register.splice(i,1);
							}				
 						}

 						if (isOdd(register[4]) && (register[5]==1)) {
 							if (register[i]>0) {
								register.splice(i,1);
							}				
 						}

 						if (isOdd(register[4]) && (register[5]==2)) {
 							if (register[i]>1) {
								register.splice(i,1);
							}				
 						}
					}
					if (isNaN(register[i])){
						register.splice(i,1);	
					}
					break;
				case 8:
				case 9:				
					if (isNaN(register[i])){
						register.splice(i,1);	
					}
					
			}

		}


		document.getElementById('register').value = register.join("");
		
	}
</script>

