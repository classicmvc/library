<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${readers.numberOfElements eq 0}">
	    <table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th  style="width: 1px;">№</th> -->
					<th>Овог</th>
					<th>Нэр</th>
					<th>Регистр</th>
					<!-- <th>Түлхүүр үг</th> -->
					<th>Гар утас</th>
					<th>И-мейл</th>
					<th>Хаяг</th>
					<th>Төлөв</th>
					<th style="width: 1px;"></th>
				</tr>

			</thead>
		</table>	
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
		${param.basketitemid}
	</c:when> 
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th  style="width: 1px;">№</th> -->
					<th>Овог</th>
					<th>Нэр</th>
					<th>Регистр</th>
					<!-- <th>Түлхүүр үг</th> -->
					<th>Гар утас</th>
					<th>И-мейл</th>
					<th>Хаяг</th>
					<th>Төлөв</th>
					<th style="width: 1px;"></th>
				</tr>

			</thead>
			<tbody>
				<c:forEach items="${readers.content}" var="reader">
					<tr>
						<td>${reader.surname}</td>
						<td>${reader.name}</td>
						<td>${reader.register}</td>
						<td>${reader.phone}</td>
						<td>${reader.email}</td>
						<td>${reader.address}</td>
						<td>${reader.status}</td>
						<td style="white-space: nowrap;">

						<c:if test="${param.select eq 1}">
						       <a class="button	button1" style="width:40px;" href="/admin/adminbasket?readerId=${reader.id}"> 
						       	 Сонгох
							 <i class="fas fa-check-circle" style="margin-left: 5px"></i>
							</a>
					    </c:if>
					    <%-- <c:if test="${!isEmpty param.basketitemid}"> --%>
					    <c:if test="${param.basketitemid ne 0}">
						       <a class="button	button1" style="width:40px;" href="/admin/rhbook/webbasket?readerId=${reader.id}&basketitemId=${param.basketitemid}"> 
						       	 Сонгох
							 <i class="fas fa-check-circle" style="margin-left: 5px"></i>
							</a>
					    </c:if>
					    
					        <button class="button button4" onclick="editThis(${reader.id})">
								Засах<i class="far fa-file-alt" style="margin-left: 5px"></i>
							</button> 
							<button class="button button3" onclick="deleteThis(${reader.id})">
								Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
							</button>
							</td>		
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	 <c:if test="${not (readers.first && readers.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not readers.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${readers.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${readers.totalPages}">
						<li
							class="page-item <c:if test="${readers.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not readers.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${readers.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if> 
		
			</c:otherwise>
</c:choose>
		
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<p>
					Энэ уншигч <span id="deleteCount">0</span> удаа үйлчлүүлсэн байна. 
				</p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary" id="deleteLink">Дэлгэрэнгүй харах</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Хаах</button>
			</div>
		</div>

	</div>
</div>
<!-- End Modal -->


<script>

	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/reader/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function(message) {
					  if (message=="Fail") {
						  $.get('/admin/reader/'+id+'/count', function(count){
							 // alert(count + "ном ашиглагдсан байна");
							$('#deleteCount').text(count);
							$('#deleteLink').attr('href', '/admin/rhbook?readerId=' + id);
							  $("#myModal").modal();
						 });
					  }
					  else {
						  alert("Амжиллтай устлаа");
						  loadList(0, "");
					  }			  		  	   
				  }
			});	
		}				
	}
	

	var selectReader = function(id) {
		$("#readerId").val(id);
		$.get("/admin/reader/"+id, function(data) {
			$("#readers").html(data);
		});		
		$("#readers").html("");
	}
</script>













