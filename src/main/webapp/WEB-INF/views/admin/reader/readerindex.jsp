<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="d-flex justify-content-between align-items-center">
	<h1>Уншигч</h1>
	<!-- <button class="btn btn-primary" onclick="create();">Шинэ</button> -->
	<button class="button button2" onclick="create();">
		Шинээр нэмэх<i class="fas fa-notes-medical" style="margin-left: 5px"></i>
	</button>
</div>

<form class="form-inline">
	<label class="sr-only" for="inlineFormInputName2">Нэр</label> 
	<input type="text" onkeypress="handle(event)" maxlength="20" class="form-control mb-2 mr-sm-2" id="filterName"
		placeholder="Хайлтын утга оруулна уу">
	<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
</form>

<div id="list"></div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>

<script>
	function handle(e){
    	if(e.keyCode === 13){
        	e.preventDefault(); // Ensure it is only this code that rusn
         	searchList(0, "");
   		 }
	} 

	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, name) {
		$.get("/admin/reader/list?select=${param.select}&basketitemid=${param.basketitemid}&page=" + page
				+ "&size=5&name=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");

	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/reader/new", function(data) {
			$("#edit").html(data);
		});

	}

	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/reader/" + id + "/readeredit", function(data) {
			$("#edit").html(data);
		})
	}

	var submitForm = function() {
		if (document.getElementById("surname").value.trim() == '') {
			alert("Овог оруулна уу");
			$("#surname").focus();
			return;
		}

		if (document.getElementById("name").value.trim() == '') {
			alert("Нэр оруулна уу");
			$("#name").focus();
			return;
		}

		if (document.getElementById("register").value.trim() == '') {
			alert("Регистер оруулна уу");
			$("#register").focus();
			return;
		}

		if (document.getElementById("phone").value.trim() == '') {
			alert("Гар утасны дугаар оруулна уу");
			$("#phone").focus();
			return;
		}

		if (document.getElementById("email").value.trim() == '') {
			alert("email оруулна уу");
			$("#email").focus();
			return;
		}

		if (document.getElementById("address").value.trim() == '') {
			alert("Хаяг оруулна уу");
			$("#address").focus();
			return;
		}

		$.post("/admin/reader/save", $('#editForm').serialize(), function() {
			$('#editModal').modal('hide');
			$("#edit").html('');
			loadList(0, "");
		});
	}
</script>