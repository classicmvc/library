<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row justify-content-center">
	<div class="col-8">

		<div class="d-flex justify-content-between align-items-center">
			<h1>Ангилал</h1>
			<!-- <button class="btn btn-primary" onclick="create();">Шинэ</button> -->

			<button class="button button2" onclick="create();">
				Шинээр нэмэх<i class="fas fa-notes-medical" style="margin-left: 5px"></i>
			</button>
		</div>

		<form class="form-inline">
			<label class="sr-only" for="inlineFormInputName2">Нэр</label> 
			<input type="text" class="form-control mb-2 mr-sm-2" id="filterName"
				maxlength="20" placeholder="20 тэмдэгтээс хэтрэхгүй" onkeypress="handle(event)"> 
			<button type="button" class="btn btn-dark mb-2"
				onclick="searchList();">Хайх</button>
		</form>

		<div id="list"></div>

	</div>
</div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>



<script>

function handle(e){
    if(e.keyCode === 13){
        e.preventDefault(); // Ensure it is only this code that rusn
         searchList(0, "");
    }
} 

	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	var loadList = function(page, name) {
		$.get("/admin/category/list?page=" + page + "&size=5&name=" + name,
				function(data) {
					$("#list").html(data);
				});
	}
	loadList(0, "");

	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/category/new", function(data) {
			$("#edit").html(data);
		});
	}

	var submitForm = function() {
		$.post("/admin/category/save", $('#editcategoryForm').serialize(),function(data) {
					$("#edit").html(data);					
				});
	}

	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/category/" + id + "/categoryedit", function(data) {
			$("#edit").html(data);
		})
	}
	
</script>