<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Ангилал нэмэх/засах</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<form:form modelAttribute="jspform" id="editcategoryForm">
			<form:hidden path="id" />
			<div class="form-group">
				<form:label path="name">Нэр</form:label>
				<form:input path="name" cssClass="form-control"
					cssErrorClass="form-control is-invalid" />
				<form:errors path="name" cssClass="invalid-feedback" element="div" />
			</div>

     <%-- ${jspform.parentId} --%>
			<div class="form-group">
			<form:label path="parentId">Үндсэн бүлэг сонгоно уу</form:label>			
				<form:select path="parentId">
				    
					<option value="">...</option>
					<c:forEach items="${categories}" var="ca">
						<form:option value="${ca.id}">${ca.name}</form:option>
					</c:forEach>
				</form:select>
			</div>
			
			<div class="form-group">
			<form:label path="ordering">Эрэмбэ сонгоно уу</form:label>
				<form:select path="ordering">
					<c:forEach  var="order" begin="1" end="100">
						<form:option value="${order}">${order}</form:option>
					</c:forEach>
				</form:select>
			</div>
			
			
			<div class="form-group">
				<form:label path="status">Төлвөө сонгоно уу</form:label>
				<form:select path="status">
					<form:option value="Идэвхтэй">Идэвхтэй</form:option>
					<form:option value="Идэвхгүй">Идэвхгүй</form:option>
				</form:select>
			</div>
		</form:form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>

<script>
	var selectCategory = function(id) {
		$("#categoryId").val(id);
		$.get("/admin/category/select/" + id, function(data) {
			$("#category").html(data);
		});
		$("#categories").html("");
	}

	<c:if test="${status eq 'success'}">
	$('#editModal').modal('hide');
	loadList(0, "");

	</c:if>
</script>
