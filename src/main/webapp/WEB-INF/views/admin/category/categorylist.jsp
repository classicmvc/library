<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${empty categories.content}">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Бүлэг</th>
					<th>Дэд бүлэг</th>
					<th>Төлөв</th>
					
					<th style="width: 1px;"></th>
				</tr>
			</thead>
		</table>	
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Бүлэг</th>
					<th>Дэд бүлэг</th>
					<th>Төлөв</th>
					
					<th style="width: 1px;"></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${categories.content}" var="category">
					<tr>
						<td>${category.parent.name}</td>
						<td>${category.name}</td>
						<td>${category.status}</td>
						
						<td style="white-space: nowrap;">
						
						<c:if test="${param.select eq 1}">
							<button class="button button1"onclick="selectCategory(${category.id}, '${category.name}')">Сонгох<i class="fas fa-check-circle" style="margin-left: 5px"></i></button>	
						</c:if>	
							<button class="button button4" onclick="editThis(${category.id})">
								Засах<i class="far fa-file-alt" style="margin-left: 5px"></i>
							</button>
							<button class="button button3" onclick="deleteThis(${category.id})">
								Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
							</button>
							</td>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		 <c:if test="${not (categories.first && categories.last)}">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not categories.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${categories.number-1});">Өмнөх</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${categories.totalPages}">
						<li
							class="page-item <c:if test="${categories.number == i-1}">active</c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not categories.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${categories.number+1});">Дараах</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if> 
		

	</c:otherwise>
</c:choose>



<!-- Modal -->
<div class="modal fade" id="deleteCategoryModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<p>
					Энэ категор <span id="cntCategoryUsed">0</span> ном дээр ашиглагдсан. 
				</p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary" id="deleteLink">Дэлгэрэнгүй харах</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Хаах</button>
			</div>
		</div>

	</div>
</div>
<!-- End Modal -->


 <script>
	var paginate = function (page) {
		loadList(page, $('#filterName').val());
	}


	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/category/" + id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success:function(message) {
					  if (message=="Fail") {
						  $.get('/admin/category/'+id+'/count', function(count){
							//  alert(count + "ном ашиглагдсан байна");
							$('#cntCategoryUsed').text(count);
							$('#deleteLink').attr('href', '/admin/book?categorId=' + id);
							  $("#deleteCategoryModal").modal();
						  });
						  
					  }
					  else {
						  alert("Амжиллтай устлаа");
						  loadList(0, "");
					  }			  
					  			  	   
				  }
			});	
		}				
	}
</script> 


