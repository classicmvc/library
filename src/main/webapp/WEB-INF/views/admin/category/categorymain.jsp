<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="row justify-content-center">
	<div class="col-8">
	<div>
		<div class="d-flex justify-content-center align-items-center">
			<h1>Ангилал</h1>
		</div>
		<div class="d-flex justify-content-end align-items-left">
			<a href="/admin/book/new" class="btn btn-secondary">Болих</a>
			<button class="btn btn-primary" onclick="create();">Шинэ</button>
		</div>
	</div>
		
	<form class="form-inline">
		<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
			type="text" class="form-control mb-2 mr-sm-2" id="filterName"
			placeholder="Нэр">
		<button type="button" class="btn btn-dark mb-2" onclick="searchList();">Хайх</button>
	</form>

		<div id="list"></div>

	</div>
</div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" id="edit"></div>
</div>



<script>
	var searchList = function() {
		loadList(0, $('#filterName').val());
	}

	
	var loadList = function(page, name) {
		$.get("/admin/category/list?select=${param.select}&page=" + page
				+ "&size=5&name=" + name, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");

	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/category/new", function(data) {
			$("#edit").html(data);
		});
	}

	var submitForm = function() {
		$.post("/admin/category/save", $('#editcategoryForm').serialize(), function() {
			$('#editModal').modal('hide');
			$("#edit").html('');
			loadList();
		});
	}

	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/category/" + id + "/categoryedit", function(data) {
			$("#edit").html(data);
		})
	}
	
</script>