<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<c:forEach items="${categories}" var="category">
	<div class="accordion">
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" type="button"
						style="color: #144271;" data-toggle="collapse"
						data-target="#collapseOne${category.id}">${category.name}</button>
				</h5>
			</div>
			<div id="collapseOne${category.id}" class="collapse">
				<div class="card-body" id="book${category.id}"></div>
			</div>
		</div>
	</div>
	
</c:forEach>

<script>
	var listBooksOfAllCategory = function(name) {
		
		<c:forEach items="${categories}" var="category">
		
		$.get("/admin/book/booklisttobasket?categoryId=${category.id}&name=" + name, function(data) {
			$("#book${category.id}").html(data);
		});
		
		</c:forEach>
		
	}
	
	listBooksOfAllCategory("");
	
		
</script>
