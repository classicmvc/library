<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

    
    <div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title">Зураг нэмэх</h5>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
	
		<form method="POST" id="fileForm" enctype="multipart/form-data" action="/admin/image/upload">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
			<input type="file" name="file"/> 
			
			<!-- <div class="form-group">
				<label for="description" >Нэр</label>
				<input name="description" id="description" class="form-control" />
			</div> -->
			
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Болих</button>
		<button type="button" class="btn btn-primary" onclick="submitForm();">Хадгалах</button>
	</div>
</div>

<div id="loadingModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog text-center text-white">Хуулж байна...</div>
</div>

<script>
var selectImage = function(id) {
	$("#imageId").val(id);
	$.get("/admin/image/select/"+id, function(data) {
		$("#image").html(data);
	});		
	$("#images").html("");
}
</script>