<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
	<c:when test="${images.numberOfElements eq 0}">
		<table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th>id</th> -->
					<th style="width: 1px;">Зураг</th>
					<th>Файлын нэр</th>
					<th>Өргөн</th>
					<th>Өндөр</th>
					<th>Хэмжээ</th>
					<th>Огноо</th>
					<th style="width: 1px;">Үйлдэл</th>
				</tr>
			</thead>
		</table>	
		<h3 class="text-secondary text-center">Мэдээлэл олдсонгүй</h3>
	</c:when>
	<c:otherwise>
		<table class="table table-bordered">
			<thead>
				<tr>
					<!-- <th>id</th> -->
					<th style="width: 1px;">Зураг</th>
					<th>Файлын нэр</th>
					<th>Өргөн</th>
					<th>Өндөр</th>
					<th>Хэмжээ</th>
					<th>Огноо</th>
					<th style="width: 1px;">Үйлдэл</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${images.content}" var="image">
					<tr>
						<%-- <td>${image.id}</td> --%>
						<td><c:if test="${!empty images}">
								<img src="/${image.path}" class="img-thumbnail rounded"
									style="max-width: 100px;" />
							</c:if></td>
						<td>${image.description}</td>
						<td>${image.width}</td>
						<td>${image.height}</td>
						<td>${image.size}</td>
						<td>${image.created}</td>
						<td style="white-space: nowrap;"><c:if
								test="${param.select eq 1}">
								<button class="btn btn-dark" type="button"
									onclick="selectImage(${image.id}, '${image.description}')">Сонгох</button>
							</c:if>
							<button class="button button4" onclick="editThis(${image.id})">
								Засах<i class="far fa-file-alt" style="margin-left: 5px"></i>
							</button> 
							<button class="button button3" onclick="deleteThis(${image.id})">
								Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
							</button>
							</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not (images.first && images.last)}">

			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:if test="${not images.first}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${images.number-1});"> <span
								aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span>
						</a></li>
					</c:if>
					<c:forEach var="i" begin="1" end="${images.totalPages}">
						<li
							class="page-item <c:if test="${images.number == i-1}"></c:if>">
							<a class="page-link" href="#" onclick="paginate(${i-1});">
								${i} </a>
						</li>
					</c:forEach>
					<c:if test="${not images.last}">
						<li class="page-item"><a class="page-link" href="#"
							onclick="paginate(${images.number+1});"> <span
								aria-hidden="true">&raquo;</span> <span class="sr-only">Next</span>
						</a></li>
					</c:if>
				</ul>
			</nav>
		</c:if>
	</c:otherwise>
</c:choose>


<!-- Modal -->
<div class="modal fade" id="deleteModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<p>
					Энэ зургийг <span id="deleteCount">0</span> ном дээр ашиглагдсан. 
				</p>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary" id="deleteLink">Дэлгэрэнгүй харах</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Хаах</button>
			</div>
		</div>

	</div>
</div>
<!-- End Modal -->

<script>
var paginate = function (page) {
	loadList(page, $('#filterName').val());
}

	var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/admin/image/" + id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function(message) {
					  if (message=="Fail") {
						  $.get('/admin/image/'+id+'/count', function(count){
							//  alert(count + "ном ашиглагдсан байна");
							$('#deleteCount').text(count);
							$('#deleteLink').attr('href', '/admin/book?imageId=' + id);
							  $("#deleteModal").modal();
						  });
						  
					  }
					  else {
						  alert("Амжиллтай устлаа");
						  loadList(0, "");
					  }			  
					  			  	   
				  }
			});	
		}				
	}
</script>
