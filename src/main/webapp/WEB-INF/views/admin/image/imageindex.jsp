<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="row justify-content-center">
	<div class="col-8">
		<div class="d-flex justify-content-between align-items-center">
			<h1>Зураг</h1>
			 <!--  <button class="btn btn-primary" onclick="create();">Шинэ</button> -->  
			
		<form method="POST" id="fileForm" enctype="multipart/form-data" action="/admin/image/upload">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
				<input type="file" name="file" onchange="submitForm();"/> 
			</form> 
			
		<!-- 	<div class="form-group" >
    <input type="file" name="img[]" class="file">
    <div class="input-group">
      <span class="input-group-btn">
      	<button class="browse btn btn-secondary input-lg" type="button" style="background-color:#F8F8F8"><i class="far fa-image" style="color:black"></i></button>
      </span>
      <input type="text" class="form-control input-lg" disabled placeholder="Upload Image" style="background-color:#F8F8F8">
      <span class="input-group-btn">
        <button class="browse btn input-lg" type="button" style="background-color: #7094db"><i class="fas fa-search"></i> Browse</button>
      </span>
    </div>
  </div>-->
			
		</div> 
		
		<form class="form-inline">
			<label class="sr-only" for="inlineFormInputName2">Нэр</label> <input
				type="text" class="form-control mb-2 mr-sm-2" id="filterName"
				placeholder="Хайлтын утгаа оруулна уу" onkeypress="handle(event)" maxlength="20">
			
			<button class="button button6" onclick="searchList()">Хайх<i class="fas fa-search" style="margin-left: 5px"></i></button>
			
			
		</form>
		
		<div id="list"></div>
	</div>
</div>

<div id="editModal" class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" id="edit"></div>
</div>


<script>

function handle(e){
    if(e.keyCode === 13){
        e.preventDefault(); // Ensure it is only this code that rusn
         searchList(0, "");
    }
} 

var searchList = function() {
	loadList(0, $('#filterName').val());
}

	/* var loadList = function(page) {
		$.get("/admin/image/list?page="+page, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0); */
	
	var loadList = function(page, description) {
		$.get("/admin/image/list?page="+page+"&size=10&description=" + description, function(data) {
			$("#list").html(data);
		});
	}
	loadList(0, "");	
	
	var submitForm = function() {		
		$('#loadingModal').modal({
			keyboard : false,
			backdrop : 'static'
		});
		$('#fileForm').submit(); 
	}
	
/* 	var create = function() {
		$('#editModal').modal('show');
		$.get("/admin/image/upload", function(data) {
			$("#edit").html(data);
		});

	}
 */
	
	var editThis = function(id) {
		$('#editModal').modal('show');
		$.get("/admin/image/" + id + "/imageedit", function(data) {
			$("#edit").html(data);
		})
	}
	
</script>