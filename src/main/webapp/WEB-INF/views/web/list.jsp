<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="books" ignore="true" />
<div class="container">
	<c:forEach var="book" items="${books.content}">
		 <a class="card mb-2 text-dark" style="text-decoration: none;">
			<div class="card-body d-flex justify-content-between" >
				<div>
					<h5 class="card-title">${book.name}</h5>
				</div>
				<c:if test="${!empty book.image}">
					<div style="margin-left: 20px;">
						<img style="height: 150px;" src="/${book.image.path}" />
					</div>
				</c:if>
			</div>
		</a>
	
		<a href="/home/${book.id}">Цааш нь үзэх</a> 
		
	</c:forEach>

	 <div class="d-flex justify-content-between mt-4">
			<c:choose>
				<c:when test="${not books.first}">
					<a href="?page=${books.number-1}" class="btn btn-info">Өмнөх</a>
				</c:when>
				<c:otherwise>
					<div></div>
				</c:otherwise>
			</c:choose>
	
			<c:if test="${not books.last}">
				<a href="?page=${books.number+1}" class="btn btn-info">Дараах</a>
			</c:if>
		</div> 
 </div>