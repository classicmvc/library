<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!-- MODAL -->

${basketitem.book.name}
<div class="modal fade" id="basketModal" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Таны Токен: ${basket.token}</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<strong><p class="lead"
							style="font-size: 15px; color: red;">
							Дээрх токеныг тэмдэглэн авч номын санчаар үйлчлүүлээрэй <span>!</span>
						</p></strong>
					<table class="table table-bordered" id="basketTable">
						<thead>
							<tr>
								<th>Захиалсан ном</th>
								<!-- <th>Токен</th>
					<th>Огноо</th>
					<th>Төлөв</th> -->
								<th style="width: 1px;">Үйлдэл</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${basketitems}" var="basketitem"
								varStatus="status">
								<tr>
									<td>${basketitem.book.name}</td>
									<td style="white-space: nowrap;">
										<button class="button button3" type="button"
											onclick="deleteThis(${basketitem.id})">
											Устгах<i class="fas fa-trash-alt" style="margin-left: 5px"></i>
										</button>
									</td>
									<td id="total" style="display: none;">${status.index+1}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="submitWebBasket();">Баталгаажуул</button>
			</div>
		</div>
	</div>
</div>

<script>
var str = $("#basketTable #total").text();

var lastChar = str[str.length - 1];
if(lastChar){
	document.getElementById("basketed").innerHTML = lastChar;
}
else{document.getElementById("basketed").innerHTML = "0";}

console.log(lastChar);

var submitWebBasket = function() {
	$.post("/home/basket/confirm?${_csrf.parameterName}=${_csrf.token}",
					function(data) {
						alert("Амжилттай хадгалагдлаа.Номын санчид хандана уу.");
						window.location = "/home"
					});
}
</script>














