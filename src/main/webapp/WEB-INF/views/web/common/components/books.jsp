<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="books" ignore="true" />

<%-- <c:forEach var="book" items="${books}">
	<div class="card">
		<c:if test="${!empty book.image}">
			<img src="/${book.image.path}" class="card-img-top" alt="${book.name}">
		</c:if>
		<div class="card-body">
			<h5 class="card-title">${book.name}</h5>
		</div>
		<a href="/home/${book.id}" class="btn btn-primary">Цааш нь үзэх</a>
	</div>
</c:forEach> --%>

<style>

	.flex-container {
	  padding: 0;
	  margin: 0;
	  list-style: none;
	  	  
	  display: -webkit-box;
	  display: -moz-box;
	  display: -ms-flexbox;
	  display: -webkit-flex;
	  display: flex;
	  
	  -webkit-flex-flow: row wrap;
	  justify-content: space-around;
	}

	.flex-item {
		margin-right: 5px;
		margin-top: 5px;
		padding: 15px;	  
		text-align: center;
		width: 200px;
	}
	.limited p{
	width: 180px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	}
</style>

<div class="flex-container">
	<c:forEach var="book" items="${books}">
		<div class="flex-item border limited">
			<img src="/${book.image.path}" height="200px;" alt="book name"> 
			<p>${book.name}</p>
			<a href="/home/${book.id}" class="btn btn-primary">Цааш нь үзэх</a>
		</div>
	</c:forEach>
</div>