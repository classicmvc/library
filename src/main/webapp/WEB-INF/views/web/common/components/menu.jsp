<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="categories" ignore="true" />

<ul class="navbar-nav">
	<c:forEach var="category" items="${categories}">
		<c:choose>
			<c:when test="${!empty category.children}">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="/home/category/${category.id}" data-hover="dropdown">${category.name}</a>
					<div class="dropdown-content">
						<c:forEach var="child" items="${category.children}">
							<a class="dropdown-item" href="/home/category/${child.id}">${child.name}</a>
						</c:forEach>
					</div>
				</li>
			</c:when>
			<c:otherwise>
				<li class="nav-item"><a class="nav-link dropdown-toggle" href="/home/category/${category.id}">${category.name}</a></li>
			</c:otherwise>
		</c:choose>

	</c:forEach>
</ul>

<div class="notification">
	<button type="button" class="btn btn-link" onclick="itemList()"
		style="font-size: 20px;">
		<span class="badge" style="background-color: #ff0023" id="basketed"></span>
		<i class="fas fa-shopping-cart"></i>
	</button>
</div>

<script>
	function filterByCategory(categoryId) {
		console.log(categoryId);
	}
</script>


