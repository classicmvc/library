<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container border">
	<div class="row">
		<div class="col-sm-4">
			<c:if test="${!empty book.image}">
				<div>
					<img src="/${book.image.path}" class="card-img" />
				</div>
			</c:if>
		</div>
		<div class="col-sm-8 ">
			<h1 class="border-bottom">${book.name}</h1>
				
			<div class="content">${book.introduction}</div>
			<hr>
				<div class="bookDes">
		          <div class="row">
			            <div class="col-sm-6 ">
			              <table>
			                <tbody>
			                  <tr>
			                    <td>Зохиолч:</td>
			                    <td>${book.author.name}</td>
			                  </tr>
			                  <tr>
			                    <td>Хэвлэсэн он:</td>
			                    <td>${book.createddate}</td>
			                  </tr>
			                  <tr>
			                    <td>Ангилал:</td>
			                    <td>${book.category.name}</td>
			                  </tr>
			                  <tr>
			                    <td>Бар код:</td>
			                     <td>${book.barcode}</td>
			                  </tr>
			                </tbody>
			              </table>
			            </div>
						 <div class="col-sm-6 ">
								<button class="button save" onclick="submitForm(${book.id});">
									Захиалах<i class="fas fa-save" style="margin-left: 5px"></i>
								</button>
					</div>
				</div>
	 		</div>
 	 </div>
</div>
</div>



<script>
var submitForm = function(bookId) {
	$.get("/home/basket/save?bookId=" + bookId, function() {
		alert("Сагсанд амжилттай хадгалагдлаа.Токеныг баталгаажуулаарай.");
		window.location = '/home';
	});
}
</script>
