<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container mt-5 mb-5">
<div class="card-deck">
	<tiles:insertTemplate template="/WEB-INF/views/web/common/components/books.jsp" preparer="bookPreparer">
		<tiles:putAttribute name="size" value="5"/>
	</tiles:insertTemplate>
</div>
	<div class="d-flex justify-content-between mt-4">
		<div></div>
		<a href="/list?page=1" class="btn btn-info">Дараах</a>
	</div>
</div>
<div id="basket"></div>



<script>
var itemList = function(){
	$('#basketModal').modal('show');
}

 $.get("/home/basket/basketlist",
		function(data) {
			$("#basket").html(data);
		});
			 
 var deleteThis = function (id) {
		if(confirm("Үнэхээр устгахыг хүсэж байна уу?")) {
			$.ajax({
				  url: "/home/basket/"+id + "?${_csrf.parameterName}=${_csrf.token}",			  			 
				  type: 'DELETE',
				  success: function() {
					  alert("Амжиллтай устлаа");
					  location.reload();
				  }
			});	
		}				
	}

</script>
