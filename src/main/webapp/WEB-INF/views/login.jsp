<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html>
<head>

<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
</head>
<style>
body {
  background-image: url("images/background.jpg");
   background-size: cover;
  /* background-size: 1900px 970px; */
}
#banner{
	text-align: center;
	color: white;
	margin-top: 260px;
}
h1{
	font-size: 80px;
}

a{
	color: white;
}
</style>
<body>
<div id="banner">
  <h1> Цахим номын сан</h1>
  <p>Цахим номын сангийн сайтад тавтай морилно уу!</p>
  
  <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#exampleModal">
   		Админ хуудас
  </button>

  <button type="button" class="btn btn-outline-light">
   		<a href="/home">Вэб сайт</a>
  </button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Нэвтрэх</h5>
				</div>
				<div class="modal-body">
					<c:if test="${error}">
						<div class="alert alert-warning" role="alert">
							Хэрэглэгчийн нэр эсвэл нууц үг буруу байна
						</div>
					</c:if>
					<form action="/login" method="POST">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						
						<div class="form-group">
							<input type="text" name="username" class="form-control" placeholder="Хэрэглэгчийн нэр" autofocus>
						</div>
						<div class="form-group">
							<input type="password" name="password" class="form-control" placeholder="Нууц үг">
						</div>
						    <button type="submit" class="btn btn-primary" style="width : 100%">Нэвтрэх</button>
					</form>
				</div>
			</div>
  </div>
</div>
</div>



	<script>
		$('#loginModal').modal({
			keyboard : false,
			backdrop : 'static'
		});
		
		
	</script>
</body>

</html>