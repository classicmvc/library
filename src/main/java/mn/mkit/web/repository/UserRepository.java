package mn.mkit.web.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import mn.mkit.web.domain.User;

public interface UserRepository  extends PagingAndSortingRepository<User, String>{
	@Query("select c from User c where c.username like %?1%")	
	Page<User> findByName(String username, Pageable pageable);
 
}