package mn.mkit.web.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Author;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Integer>{
	@Query("select a from Author a where a.name like %?1% or a.surname like %?1% or a.nationality like %?1%")	
	Page<Author> findByName(String name, Pageable pageable);
	
	@Query("select a from Author a where a.name=?1 and a.surname=?2 and a.nationality=?3 and a.birthdate=?4")
	List<Author> checkDuplicate(String name,String surname,String nationality,Date birthdate);
}
