package mn.mkit.web.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.AdminBasket;

public interface AdminBasketRepository extends PagingAndSortingRepository<AdminBasket, Integer>{
	
	@Query("select a from AdminBasket a where a.readerId=?1")
	List<AdminBasket> findByReaderId(Integer readerId);
	
	@Query("select a from AdminBasket a where a.readerId=?1 and a.book.id=?2")
	List<AdminBasket> find(Integer readerId, Integer bookId );
}


