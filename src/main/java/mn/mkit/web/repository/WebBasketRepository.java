package mn.mkit.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Basket;

public interface WebBasketRepository extends PagingAndSortingRepository<Basket, Integer>{
	
	@Query("select a from Basket a where a.sessionId=?1")
	Basket findSessionId(String sessionId);
	
	@Query("select max(e.nomer)+1 from Basket e where date(e.enterdate) = date(sysdate())")
	Integer maxNomerByToday();
	
	@Query("select a from Basket a where a.status=?1")
	List<Basket> findByStatus(String status);
}
