package mn.mkit.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Image;


public interface ImageRepository extends PagingAndSortingRepository<Image, Integer> {
	
	@Query("select e from Image e where e.description like %?1%")
	Page<Image> findByDescription(String description, Pageable pageable);
}
