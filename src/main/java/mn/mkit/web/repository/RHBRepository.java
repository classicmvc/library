package mn.mkit.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Book;
import mn.mkit.web.domain.RHBook;

public interface RHBRepository extends PagingAndSortingRepository<RHBook,Integer>{
	

	@Query("select c from RHBook c where c.reader.name like %?1% or c.book.name like %?1%")	
	List<RHBook>search(String q);
	
	@Query("select c from RHBook c where c.reader.name like %?1% or c.book.name like %?1%")	
	Page<RHBook>searchPage(String q, Pageable pageable);
	
	@Query("select a from RHBook a where a.id=?1 and (a.status='Олгосон' or a.status='Сунгагдсан')")
	List<RHBook> find(Integer Id);
	
	@Query("select count(e) from RHBook e where e.reader.id = ?1")	
	Integer countByReader(Integer readerId);
	
	@Query("select e from RHBook e where e.reader.id = ?1")	
	Page<RHBook> findByReaderId(Integer readerId, Pageable pageable);
	
	
}
