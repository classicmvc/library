package mn.mkit.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.BasketItem;

public interface WebBasketItemRepository extends PagingAndSortingRepository<BasketItem, Integer>{
	
	@Query("select a from BasketItem a where a.basket.id=?1")
	List<BasketItem> findByBasketId(Integer basketId);
	
	@Query("select a from BasketItem a where a.id=?1")
	List<BasketItem> findByWebBasket(Integer basketId);
	
	@Query("select a from BasketItem a where a.status=?1 order by enterdate")
	List<BasketItem> findByStatus(String status);

}
