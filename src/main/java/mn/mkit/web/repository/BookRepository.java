package mn.mkit.web.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import mn.mkit.web.domain.Book;

public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {

	@Query("select e from Book e where e.name like %?1% or e.barcode like %?1% or e.username like %?1% or e.introduction like %?1%")
	Page<Book> findByName(String name, Pageable pageable);
	
	@Query("select e from Book e where e.status='Идэвхтэй'")
	Page<Book> findByAllWeb(Pageable pageable);
	
	@Query("select e from Book e where e.barcode=?1")
	List<Book> findByBarcode(String barcode);
	

	@Query("select e from Book e where e.category.id = ?1 and e.name like %?2% and e.status='Идэвхтэй' ")	
	List<Book> findByCategory(Integer categoryId, String name);
	
	@Query("select count(e) from Book e where e.author.id = ?1")	
	Integer countByAuthor(Integer authorId);
	
	@Query("select count(e) from Book e where e.category.id = ?1")	
	Integer countByCategory(Integer categoryId);
	
	@Query("select count(e) from Book e where e.image.id = ?1")	
	Integer countByImage(Integer imageId);
	
	
	@Query("select e from Book e where e.author.id = ?1")	
	Page<Book> findByAuthor(Integer authorId, Pageable pageable);
	
	@Query("select e from Book e where e.name like %?1% or e.barcode like %?1% ")	
	List<Book> search(String q);
	
	@Query("select b from Book b where b.category.id=?1 and b.status='Идэвхтэй'")
	Page<Book> findByCategory(Integer categoryId, Pageable pageable);
}
