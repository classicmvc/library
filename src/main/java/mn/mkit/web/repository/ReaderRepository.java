package mn.mkit.web.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Reader;

public interface ReaderRepository extends PagingAndSortingRepository<Reader, Integer> {
	
	@Query("select d from Reader d where d.name like %?1% or d.surname like %?1% or d.register like %?1% or d.phone like %?1% or d.email like %?1% or d.address like %?1%")	
	Page<Reader> findByName(String name, Pageable pageable);

}
