package mn.mkit.web.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.mkit.web.domain.Category;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
	
	@Query("select f from Category f where f.name like %?1% and f.status='Идэвхтэй'")	
	Page<Category> findByName(String name, Pageable pageable);
	
	@Query("select f from Category f where f.parent.id is null and f.status='Идэвхтэй' order by ordering")	
	List<Category> findByParent();
	 
	 @Query("select f from Category f where f.parent.id=?1 and f.status='Идэвхтэй' order by ordering")
	 List<Category> findByChild(Integer parentId); 
	 
/*	 @Query("select f from Category f where f.name like %?1% and f.id in (select b.categoryId from books b)")	
	 List<Category> findByUsedCategory(String name, Pageable pageable);	
	 */
	 
	 @Query("select f from Category f where f.name like %?1%")	
	 List<Category> findByUsedCategory(String name, Pageable pageable);
}