package mn.mkit.web.controller.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Reader;
import mn.mkit.web.modal.ReaderForm;
import mn.mkit.web.repository.RHBRepository;
import mn.mkit.web.repository.ReaderRepository;

@Controller
@RequestMapping(value = "/admin/reader")
public class ReaderAdminController {
	
	@Autowired
	private ReaderRepository readerRepository;
	
	@Autowired
	private RHBRepository rhbRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_READER_MAIN')")		
	@GetMapping
	public String main () {		
		return "admin/reader";		
	}
	

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_READER_LIST')")
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		model.addAttribute("readers", readerRepository.findByName(name == null ? "" : name, pageable));		
		return "admin/reader/readerlist";	
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_READER_DELETE')")	
	@DeleteMapping("{id}")
	@ResponseBody
	//@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public String delete(@PathVariable Integer id) {
		Reader reader = readerRepository.findById(id).get();
		try {
			readerRepository.delete(reader);
			return "Success";
		}
		catch (Exception e) {
			return "Fail";
		}		
	}
	
    @GetMapping("{id}/count")
	@ResponseBody	
	public Integer getCount(@PathVariable Integer id) {
		return rhbRepository.countByReader(id);		
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_READER_SAVE')")		
	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(ReaderForm form) throws Exception {
		Reader reader;
		if (form.getId()==null) {
			reader = new Reader();
			}
		else {
			reader = readerRepository.findById(form.getId()).get();
		}		
		reader.setSurname(form.getSurname());	
		reader.setName(form.getName());	
		reader.setRegister(form.getRegister());	
		reader.setPhone(form.getPhone());
		reader.setEmail(form.getEmail());	
		reader.setAddress(form.getAddress());	
		reader.setStatus(form.getStatus());	
		readerRepository.save(reader);		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_READER_EDIT')")
	@GetMapping("new")
	public String newForm (Model model) {		
		model.addAttribute("jspform", new ReaderForm());
		return "admin/reader/readeredit";	
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_READER_EDIT')")	
	@GetMapping("{id}/readeredit")
	public String edit (@PathVariable Integer id, Model model) {
		Reader reader = readerRepository.findById(id).get();
		ReaderForm readerForm = new ReaderForm();
		readerForm.setId(reader.getId());
		readerForm.setSurname(reader.getSurname());
		readerForm.setName(reader.getName());
		readerForm.setRegister(reader.getRegister());
		readerForm.setPhone(reader.getPhone());
		readerForm.setEmail(reader.getEmail());
		readerForm.setAddress(reader.getAddress());
		readerForm.setStatus(reader.getStatus());
		model.addAttribute("jspform", readerForm);
		return "admin/reader/readeredit";	
	}
	
			
}
