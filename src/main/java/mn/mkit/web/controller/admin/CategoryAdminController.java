package mn.mkit.web.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import mn.mkit.web.domain.Category;
import mn.mkit.web.modal.CategoryForm;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.CategoryRepository;

@Controller
@RequestMapping(value = "/admin/category")
public class CategoryAdminController {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private BookRepository bookRepository; 

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_CATEGORY_INDEX')")
	@GetMapping
	public String index() {
		return "admin/category";
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_CATEGORY_MAIN')")
	@GetMapping("main")
	public String main() {
		return "admin/category/main";
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_CATEGORY_LIST')")
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		model.addAttribute("categories", categoryRepository.findByName(name == null ? "" : name, pageable));
		return "admin/category/categorylist";
	}

	@GetMapping("categorylisttobasket")
	public String listbasket(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		//model.addAttribute("categories", categoryRepository.findAll());
		model.addAttribute("categories",categoryRepository.findByUsedCategory(name == null ? "" : name, pageable));
		return "admin/category/categorylisttobasket";
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_CATEGORY_DELETE')")
	@DeleteMapping("{id}")
	//@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@ResponseBody
	public String delete(@PathVariable Integer id){
		Category category = categoryRepository.findById(id).get();
		try
		{
			categoryRepository.delete(category);
			return "Success";
		}
		catch (Exception e) 
		{
			return "Fail";
		}
		
	}

	
	@GetMapping("{id}/count")
	@ResponseBody	
	public Integer getCount(@PathVariable Integer id) {
		return 	bookRepository.countByCategory(id);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_CATEGORY_SAVE')")
	@PostMapping("save")
	public String save(@Valid @ModelAttribute("jspform") CategoryForm form, BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("status", "error");
			model.addAttribute("jspform", form);
			return "admin/category/categoryedit";
		}
		Category category;
		if (form.getId() == null) {
			category = new Category();
		} else {
			category = categoryRepository.findById(form.getId()).get();
		}

		category.setOrdering(form.getOrdering());
		category.setName(form.getName());
		category.setStatus(form.getStatus());
		category.setParent(form.getParentId() == null ? null : categoryRepository.findById(form.getParentId()).get());
		categoryRepository.save(category);
		model.addAttribute("status", "success");
		return "admin/category/categoryedit";
	}

	@GetMapping("new")
	public String newForm(Model model) {
		model.addAttribute("jspform", new CategoryForm());
		model.addAttribute("categories", categoryRepository.findByParent());
		return "admin/category/categoryedit";
	}

	@GetMapping("categoryselected/{id}")
	public String getSelected(Model model, @PathVariable Integer id) {
		model.addAttribute("category", categoryRepository.findById(id).get());
		return "admin/category/categoryselected";
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_CATEGORY_EDIT')")
	@GetMapping("{id}/categoryedit")
	public String edit(@PathVariable Integer id, Model model) {
		Category category = categoryRepository.findById(id).get();
		CategoryForm categoryForm = new CategoryForm();
		categoryForm.setId(category.getId());
		categoryForm.setName(category.getName());
		categoryForm.setStatus(category.getStatus());
		categoryForm.setOrdering(category.getOrdering());
		categoryForm.setParentId(category.getParent() == null ? null : category.getParent().getId());
		model.addAttribute("jspform", categoryForm);
		model.addAttribute("categories", categoryRepository.findByParent());
		return "admin/category/categoryedit";
	}

}
