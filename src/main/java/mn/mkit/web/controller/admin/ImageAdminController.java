package mn.mkit.web.controller.admin;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import mn.mkit.web.business.FileService;
import mn.mkit.web.domain.Image;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.ImageRepository;

@Controller
@RequestMapping(value="/admin/image")
public class ImageAdminController {
	
	@Autowired
	private FileService service;
	
	@Autowired
	private ImageRepository repo;
	
	@Autowired
	private BookRepository bookRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_IMAGE_INDEX')")	
	@GetMapping
	public String index () {		
		return "admin/image";		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_IMAGE_MAIN')")	
	@GetMapping("main")
	public String admin () {		
		return "admin/image/main";		
	}
	
	
	@PostMapping("upload")
	public String uploadFile(@RequestParam MultipartFile file) throws Exception{
		if (service.isImageFile(file)) {
			Image image = service.uploadImage(file);
			image.setCreated(new Date());
			repo.save(image);			
		}
		return "redirect:/admin/image";
	}
		
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_IMAGE_LIST')")		
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String description, Pageable pageable) {		
		model.addAttribute("images", repo.findByDescription(description == null ? "" : description, pageable));
		return "admin/image/imagelist";		
	}
	
	

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_IMAGE_DELETE')")
	@DeleteMapping("{id}")
	//@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@ResponseBody
	public String delete(@PathVariable Integer id) {
		Image image = repo.findById(id).get();
		try
		{
			repo.delete(image);
			return "Success";
		}
		catch (Exception e) 
		{			
			return "Fail";
		}	
		
	}
	
	
	@GetMapping("{id}/count")
	@ResponseBody	
	public Integer getCount(@PathVariable Integer id) {
		return bookRepository.countByImage(id);		
	}
	
	
	@GetMapping("imageselected/{id}")
	public String getSelected (Model model, @PathVariable Integer id) {		
		model.addAttribute("image", repo.findById(id).get());
		return "admin/image/imageselected";		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_IMAGE_EDIT')")	
	@GetMapping("{id}/imageedit")
	public String edit (@PathVariable Integer id, Model model) {
		Image image = repo.findById(id).get();
		image.setId(image.getId());
		image.setPath(image.getPath());
		image.setDescription(image.getDescription());
		image.setWidth(image.getWidth());
		image.setHeight(image.getHeight());
		image.setSize(image.getSize());
        image.setCreated(image.getCreated());

		
		model.addAttribute("jspform", image);
		return "admin/image/imageedit";	
	}
	
}
