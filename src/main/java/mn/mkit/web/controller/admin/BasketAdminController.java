package mn.mkit.web.controller.admin;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.AdminBasket;
import mn.mkit.web.domain.Basket;
import mn.mkit.web.domain.BasketItem;
import mn.mkit.web.domain.Book;
import mn.mkit.web.domain.RHBook;
import mn.mkit.web.domain.Reader;
import mn.mkit.web.repository.AdminBasketRepository;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.RHBRepository;
import mn.mkit.web.repository.ReaderRepository;
import mn.mkit.web.repository.WebBasketItemRepository;
import mn.mkit.web.repository.WebBasketRepository;

@Controller
@RequestMapping(value = "/admin/adminbasket")
public class BasketAdminController {

	@Autowired
	private ReaderRepository readerRepository;
	
	@Autowired
	private AdminBasketRepository basketRepository;

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private WebBasketRepository wbrepo;
	
	@Autowired
	private WebBasketItemRepository wbitemrepo;
	
	@Autowired
	private RHBRepository rhbrepository;


	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_ADMINBASKET_MAIN')")
	@GetMapping
	public String main(@RequestParam Integer readerId, Map<String, Object> model) {
		model.put("adminBasket", readerRepository.findById(readerId).get());
		return "admin/adminbasket";
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_ADMINBASKET_LIST')")
	@GetMapping("adminbasketlist")
	public String list(@RequestParam Integer readerId, Model model) {
		List<AdminBasket> adminbaskets = basketRepository.findByReaderId(readerId);
		model.addAttribute("adminbaskets", adminbaskets);
		return "admin/adminbasket/adminbasketlist"; 
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_ADMINBASKET_SAVE')")
	@GetMapping("save")
	@ResponseBody
	public String save(@RequestParam Integer readerId, @RequestParam Integer bookId) {
		AdminBasket adminbasket = new AdminBasket();
		adminbasket.setReaderId(readerId);
		adminbasket.setBook(bookRepository.findById(bookId).get());

			
		if (basketRepository.find(readerId, bookId).isEmpty()) {
			basketRepository.save(adminbasket);
			
			Book book = bookRepository.findById(bookId).get();
			book.setStatus("Захиалсан");
			bookRepository.save(book);
			
			return "success";
		} else {
			return "duplicate";
		}		
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_ADMINBASKET_DELETE')")
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		AdminBasket adminbasket = basketRepository.findById(id).get();
		basketRepository.delete(adminbasket);
	}

	@DeleteMapping("webbasket/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deletewebbasket(@PathVariable Integer id) {
		BasketItem basketItem = wbitemrepo.findById(id).get();
		wbitemrepo.delete(basketItem);

		Basket wbasket = wbrepo.findById(basketItem.getBasket().getId()).get();
		
		if (wbitemrepo.findByBasketId(wbasket.getId()).isEmpty()) {
			wbrepo.delete(wbasket);
		}
	}
}
