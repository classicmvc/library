package mn.mkit.web.controller.admin;

import java.security.Principal;
import java.time.LocalDate;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Book;
import mn.mkit.web.modal.BookForm;
import mn.mkit.web.repository.AuthorRepository;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.CategoryRepository;
import mn.mkit.web.repository.ImageRepository;

@Controller
@RequestMapping(value = "admin/book")
public class BookAdminController {

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private AuthorRepository authorRepository;

	@Autowired
	private ImageRepository imageRepository;

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_BOOK_MAIN')")
	@GetMapping
	public String main() {
		return "admin/book";

	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_BOOK_LIST')")
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, @RequestParam(required = false) Integer authorId, Pageable pageable) {
		if (authorId == null) {
			model.addAttribute("books", bookRepository.findByName(name == null ? "" : name, pageable));			
		}
		else {
			System.out.println(authorId);
			model.addAttribute("books", bookRepository.findByAuthor(authorId, pageable));
		}	
		return "admin/book/booklist";
	}
	
	@GetMapping("booklisttobasket")
	public String listbasket (Model model, @RequestParam(required = false) String name,@RequestParam Integer categoryId) {		
		model.addAttribute("books", bookRepository.findByCategory(categoryId, (name == null ? "" : name)));
		return "admin/book/booklisttobasket";		
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_BOOK_DELETE')")
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		Book book = bookRepository.findById(id).get();
		bookRepository.delete(book);
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_BOOK_SAVE')")
	@PostMapping("save")
	public String save(@Valid @ModelAttribute("jspform")BookForm form,BindingResult bindingResult, Model model , Principal principal) throws Exception {

		if (bindingResult.hasErrors()) {
			model.addAttribute("jspform", form);
			model.addAttribute("books", bookRepository.findAll());
			return "admin/book/bookedit";
		}
		
		Book book;
		Boolean is_edit;
		
		if (form.getId() == null) {
			book = new Book();
		 	is_edit=false;
		} else {
			book = bookRepository.findById(form.getId()).get();
			is_edit=true;
		}
		
		book.setId(form.getId());
		book.setName(form.getName());
		book.setBarcode(form.getBarcode());
		book.setUsername(principal.getName());
		book.setStatus(form.getStatus());
		book.setPrinteddate(form.getPrinteddate());
		book.setCreateddate(LocalDate.now());
		book.setIntroduction(form.getIntroduction());
		book.setCategory(form.getCategoryId() == null ? null : categoryRepository.findById(form.getCategoryId()).get());
		book.setAuthor(form.getAuthorId() == null ? null : authorRepository.findById(form.getAuthorId()).get());
		book.setImage(form.getImageId() == null ? null : imageRepository.findById(form.getImageId()).get());

		
		if (bookRepository.findByBarcode(form.getBarcode()).isEmpty()) {
			bookRepository.save(book);
			return "redirect:/admin/book";
		} else {
			if (is_edit==true) {
				bookRepository.save(book);
				return "redirect:/admin/book";
			}
			else {
				bindingResult.addError(new FieldError("jspform", "barcode", "Баар код давхацсан байна"));
			}
		}
		return "admin/book/bookedit";
	}

	@GetMapping("new")
	public String newForm(Map<String, Object> model) {
		model.put("jspform", new BookForm());
		model.put("categories", categoryRepository.findAll());
		model.put("authors", authorRepository.findAll());
		return "admin/book/bookedit";

	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_BOOK_EDIT')")
	@GetMapping("{id}/bookedit")
	public String edit(@PathVariable Integer id, Map<String, Object> model) {
		Book book = bookRepository.findById(id).get();
		BookForm bookForm = new BookForm();
		bookForm.setId(book.getId());
		bookForm.setName(book.getName());
		bookForm.setBarcode(book.getBarcode());
		bookForm.setStatus(book.getStatus());
		bookForm.setPrinteddate(book.getPrinteddate());
		bookForm.setIntroduction(book.getIntroduction());
		bookForm.setCategoryId(book.getCategory() == null ? null : book.getCategory().getId());
		bookForm.setCategoryName(book.getCategory() == null ? null : book.getCategory().getName());
		bookForm.setAuthorId(book.getAuthor() == null ? null : book.getAuthor().getId());
		bookForm.setAuthorName(book.getAuthor() == null ? null : book.getAuthor().getName());
		bookForm.setImageId(book.getImage() == null ? null : book.getImage().getId());
		bookForm.setDescription(book.getImage() == null ? null : book.getImage().getDescription());
		model.put("jspform", bookForm);
		model.put("categories", categoryRepository.findAll());
		model.put("authors", authorRepository.findAll());
		return "admin/book/bookedit";

	}
}
