package mn.mkit.web.controller.admin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Authority;
import mn.mkit.web.domain.User;
import mn.mkit.web.modal.UserForm;
import mn.mkit.web.repository.AuthorityRepository;
import mn.mkit.web.repository.UserRepository;

@Controller
@RequestMapping(value = "/admin/user")
public class UserAdminController {

	@Autowired
	private UserRepository repo;

	@Autowired
	private AuthorityRepository authorityrepository;

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_USER_MAIN')")	
	@GetMapping
	public String main() {
		return "admin/user";
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_USER_LIST')")	
	@GetMapping("list")
	public String list(Model model, String username, Pageable pageable) {
		model.addAttribute("users", repo.findByName(username, pageable));
		return "admin/user/userlist";
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_USER_DELETE')")	
	@DeleteMapping("{username}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String username) {
		User user = repo.findById(username).get();
		repo.delete(user);

	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_USER_SAVE')")	
	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(@RequestParam String username,UserForm form) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		User user = new User();

		user.setUsername(form.getUsername());
		
			user.setPassword(encoder.encode(form.getPassword()));
			user.setEmail(form.getEmail());
			user.setPhone(form.getPhone());
			user.setEnabled(form.getEnabled());
			repo.save(user);
			List<Authority> authorities = authorityrepository.findByUsername(username);
			authorityrepository.deleteAll(authorities);

			Authority admin = new Authority();
			admin.setUsername(user.getUsername());
			admin.setAuthority("ROLE_ADMIN");
			authorityrepository.save(admin);

		
			for (String role : form.getAuthorities()) {
				Authority authority = new Authority();
				authority.setUsername(user.getUsername());
				authority.setAuthority(role);
				authorityrepository.save(authority);
			}
	}

	@GetMapping("new")
	public String newForm(Model model) {
		model.addAttribute("jspform", new UserForm());

		List<List<String>> roles = new ArrayList<>();
		
		List<String> readerRoles = new ArrayList<>();
		readerRoles.add("READER");
		readerRoles.add("ROLE_READER_EDIT");
		readerRoles.add("ROLE_READER_DELETE");
		readerRoles.add("ROLE_READER_SAVE");
		readerRoles.add("ROLE_READER_UPDATE");
		readerRoles.add("ROLE_READER_LIST");
		readerRoles.add("ROLE_READER_MAIN");
		readerRoles.add("ROLE_READER_INDEX");
		roles.add(readerRoles);
		
		List<String> bookRoles = new ArrayList<>();
		bookRoles.add("BOOK");
		bookRoles.add("ROLE_BOOK_EDIT");
		bookRoles.add("ROLE_BOOK_DELETE");
		bookRoles.add("ROLE_BOOK_SAVE");
		bookRoles.add("ROLE_BOOK_UPDATE");
		bookRoles.add("ROLE_BOOK_LIST");
		bookRoles.add("ROLE_BOOK_MAIN");
		bookRoles.add("ROLE_BOOK_INDEX");
		roles.add(bookRoles);
		
		List<String> imageRoles = new ArrayList<>();
		imageRoles.add("IMAGE");
		imageRoles.add("ROLE_IMAGE_EDIT");
		imageRoles.add("ROLE_IMAGE_DELETE");
		imageRoles.add("ROLE_IMAGE_SAVE");
		imageRoles.add("ROLE_IMAGE_UPDATE");
		imageRoles.add("ROLE_IMAGE_LIST");
		imageRoles.add("ROLE_IMAGE_MAIN");
		imageRoles.add("ROLE_IMAGE_INDEX");
		roles.add(imageRoles);
		
		List<String> rhbookRoles = new ArrayList<>();
		rhbookRoles.add("RHBOOK");
		rhbookRoles.add("ROLE_RHBOOK_EDIT");
		rhbookRoles.add("ROLE_RHBOOK_DELETE");
		rhbookRoles.add("ROLE_RHBOOK_SAVE");
		rhbookRoles.add("ROLE_RHBOOK_UPDATE");
		rhbookRoles.add("ROLE_RHBOOK_LIST");
		rhbookRoles.add("ROLE_RHBOOK_MAIN");
		rhbookRoles.add("ROLE_RHBOOK_INDEX");
		roles.add(rhbookRoles);
		
		List<String> categoryRoles = new ArrayList<>();
		categoryRoles.add("CATEGORY");
		categoryRoles.add("ROLE_CATEGORY_EDIT");
		categoryRoles.add("ROLE_CATEGORY_DELETE");
		categoryRoles.add("ROLE_CATEGORY_SAVE");
		categoryRoles.add("ROLE_CATEGORY_UPDATE");
		categoryRoles.add("ROLE_CATEGORY_LIST");
		categoryRoles.add("ROLE_CATEGORY_MAIN");
		categoryRoles.add("ROLE_CATEGORY_INDEX");
		roles.add(categoryRoles);
		
		List<String> authorRoles = new ArrayList<>();
		authorRoles.add("AUTHOR");
		authorRoles.add("ROLE_AUTHOR_EDIT");
		authorRoles.add("ROLE_AUTHOR_DELETE");
		authorRoles.add("ROLE_AUTHOR_SAVE");
		authorRoles.add("ROLE_AUTHOR_UPDATE");
		authorRoles.add("ROLE_AUTHOR_LIST");
		authorRoles.add("ROLE_AUTHOR_MAIN");
		authorRoles.add("ROLE_AUTHOR_INDEX");
		roles.add(authorRoles);
			
		List<String> basketRoles = new ArrayList<>();
		basketRoles.add("ADMINBASKET");
		basketRoles.add("ROLE_ADMINBASKET_EDIT");
		basketRoles.add("ROLE_ADMINBASKET_DELETE");
		basketRoles.add("ROLE_ADMINBASKET_SAVE");
		basketRoles.add("ROLE_ADMINBASKET_UPDATE");
		basketRoles.add("ROLE_ADMINBASKET_LIST");
		basketRoles.add("ROLE_ADMINBASKET_MAIN");
		basketRoles.add("ROLE_ADMINBASKET_INDEX");
		roles.add(basketRoles);
		

		List<String> userRoles = new ArrayList<>();
		userRoles.add("USER");
		userRoles.add("ROLE_USER_EDIT");
		userRoles.add("ROLE_USER_DELETE");
		userRoles.add("ROLE_USER_SAVE");
		userRoles.add("ROLE_USER_UPDATE");
		userRoles.add("ROLE_USER_LIST");
		userRoles.add("ROLE_USER_MAIN");
		userRoles.add("ROLE_USER_INDEX");
		roles.add(userRoles);
		

		model.addAttribute("roles", roles);
		return "admin/user/useredit";
	}
	
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_USER_EDIT')")	
	@GetMapping("{username}/useredit")
	public String edit(@PathVariable String username, Model model) {
		User user = repo.findById(username).get();
		UserForm form = new UserForm();		
		form.setUsername(user.getUsername());
		form.setPassword(user.getPassword());
		form.setEmail(user.getEmail());
		form.setPhone(user.getPhone());
		form.setEnabled(user.getEnabled());
		
		
		List<Authority> authorities = authorityrepository.findByUsername(username);
		List<String> roleString = new ArrayList<>();
		
		for(Authority authority: authorities){
			roleString.add(authority.getAuthority());
		}
		form.setAuthorities(roleString);
		
		model.addAttribute("jspform", form);
		
		List<List<String>> roles = new ArrayList<>();
		
		List<String> readerRoles = new ArrayList<>();
		readerRoles.add("READER");
		readerRoles.add("ROLE_READER_EDIT");
		readerRoles.add("ROLE_READER_DELETE");
		readerRoles.add("ROLE_READER_SAVE");
		readerRoles.add("ROLE_READER_UPDATE");
		readerRoles.add("ROLE_READER_LIST");
		readerRoles.add("ROLE_READER_MAIN");
		readerRoles.add("ROLE_READER_INDEX");
		roles.add(readerRoles);
		
		List<String> bookRoles = new ArrayList<>();
		bookRoles.add("BOOK");
		bookRoles.add("ROLE_BOOK_EDIT");
		bookRoles.add("ROLE_BOOK_DELETE");
		bookRoles.add("ROLE_BOOK_SAVE");
		bookRoles.add("ROLE_BOOK_UPDATE");
		bookRoles.add("ROLE_BOOK_LIST");
		bookRoles.add("ROLE_BOOK_MAIN");
		bookRoles.add("ROLE_BOOK_INDEX");
		roles.add(bookRoles);
		
		List<String> imageRoles = new ArrayList<>();
		imageRoles.add("IMAGE");
		imageRoles.add("ROLE_IMAGE_EDIT");
		imageRoles.add("ROLE_IMAGE_DELETE");
		imageRoles.add("ROLE_IMAGE_SAVE");
		imageRoles.add("ROLE_IMAGE_UPDATE");
		imageRoles.add("ROLE_IMAGE_LIST");
		imageRoles.add("ROLE_IMAGE_MAIN");
		imageRoles.add("ROLE_IMAGE_INDEX");
		roles.add(imageRoles);
		
		List<String> rhbookRoles = new ArrayList<>();
		rhbookRoles.add("RHBOOK");
		rhbookRoles.add("ROLE_RHBOOK_EDIT");
		rhbookRoles.add("ROLE_RHBOOK_DELETE");
		rhbookRoles.add("ROLE_RHBOOK_SAVE");
		rhbookRoles.add("ROLE_RHBOOK_UPDATE");
		rhbookRoles.add("ROLE_RHBOOK_LIST");
		rhbookRoles.add("ROLE_RHBOOK_MAIN");
		rhbookRoles.add("ROLE_RHBOOK_INDEX");
		roles.add(rhbookRoles);
		
		List<String> categoryRoles = new ArrayList<>();
		categoryRoles.add("CATEGORY");
		categoryRoles.add("ROLE_CATEGORY_EDIT");
		categoryRoles.add("ROLE_CATEGORY_DELETE");
		categoryRoles.add("ROLE_CATEGORY_SAVE");
		categoryRoles.add("ROLE_CATEGORY_UPDATE");
		categoryRoles.add("ROLE_CATEGORY_LIST");
		categoryRoles.add("ROLE_CATEGORY_MAIN");
		categoryRoles.add("ROLE_CATEGORY_INDEX");
		roles.add(categoryRoles);
		
		List<String> authorRoles = new ArrayList<>();
		authorRoles.add("AUTHOR");
		authorRoles.add("ROLE_AUTHOR_EDIT");
		authorRoles.add("ROLE_AUTHOR_DELETE");
		authorRoles.add("ROLE_AUTHOR_SAVE");
		authorRoles.add("ROLE_AUTHOR_UPDATE");
		authorRoles.add("ROLE_AUTHOR_LIST");
		authorRoles.add("ROLE_AUTHOR_MAIN");
		authorRoles.add("ROLE_AUTHOR_INDEX");
		roles.add(authorRoles);
			
		List<String> basketRoles = new ArrayList<>();
		basketRoles.add("ADMINBASKET");
		basketRoles.add("ROLE_ADMINBASKET_EDIT");
		basketRoles.add("ROLE_ADMINBASKET_DELETE");
		basketRoles.add("ROLE_ADMINBASKET_SAVE");
		basketRoles.add("ROLE_ADMINBASKET_UPDATE");
		basketRoles.add("ROLE_ADMINBASKET_LIST");
		basketRoles.add("ROLE_ADMINBASKET_MAIN");
		basketRoles.add("ROLE_ADMINBASKET_INDEX");
		roles.add(basketRoles);
		

		List<String> userRoles = new ArrayList<>();
		userRoles.add("USER");
		userRoles.add("ROLE_USER_EDIT");
		userRoles.add("ROLE_USER_DELETE");
		userRoles.add("ROLE_USER_SAVE");
		userRoles.add("ROLE_USER_UPDATE");
		userRoles.add("ROLE_USER_LIST");
		userRoles.add("ROLE_USER_MAIN");
		userRoles.add("ROLE_USER_INDEX");
		roles.add(userRoles);
		

		model.addAttribute("roles", roles);
											
		return "admin/user/useredit";
	}
}