package mn.mkit.web.controller.admin;


import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import mn.mkit.web.domain.Author;
import mn.mkit.web.modal.AuthorForm;
import mn.mkit.web.repository.AuthorRepository;
import mn.mkit.web.repository.BookRepository;

@Controller
@RequestMapping(value = "/admin/author")
public class AuthorAdminController {

	@Autowired
	private AuthorRepository authorRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_AUTHOR_INDEX')")		
	@GetMapping
	public String index () {		
		return "admin/author";		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_AUTHOR_MAIN')")	
	@GetMapping("main")
	public String admin () {		
		return "admin/author/main";		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_AUTHOR_LIST')")
	@GetMapping("list")
	public String list(Model model, @RequestParam(required = false) String name, Pageable pageable) {
		model.addAttribute("authors", authorRepository.findByName(name == null ? "" : name, pageable));		
		return "admin/author/authorlist";	
		
	}	
		
	@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_AUTHOR_SAVE')")	
	@PostMapping("save")	
	@ResponseBody
	public String save(AuthorForm form) throws Exception {
		
		Author author;
		if (form.getId()==null) {
			author 
			= new Author();
		}
		else {
		author = authorRepository.findById(form.getId()).get();
		}		
		author.setSurname(form.getSurname());	
		author.setName(form.getName());	
		author.setNationality(form.getNationality());	
		author.setBirthdate(new SimpleDateFormat("yyyy-MM-dd").parse(form.getBirthdate()));
		
		if (form.getId()==null) {
			if (authorRepository.checkDuplicate(form.getName(), form.getSurname(), form.getNationality(), new SimpleDateFormat("yyyy-MM-dd").parse(form.getBirthdate())).isEmpty()) 
			{authorRepository.save(author);	
				return "success";
			}
		else {
			return "duplicate";
		   }
		}
		else {
			authorRepository.save(author);	
			return "success";
		}
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_AUTHOR_DELETE')")	
	@DeleteMapping("{id}")
	@ResponseBody
	public String delete(@PathVariable Integer id) {
		Author author = authorRepository.findById(id).get();
		try
		{
			authorRepository.delete(author);
			return "Success";
		}
		catch (Exception e) 
		{			
			return "Fail";
		}	
	}
	
	@GetMapping("{id}/count")
	@ResponseBody	
	public Integer getCount(@PathVariable Integer id) {
		return bookRepository.countByAuthor(id);		
	}
	

	@GetMapping("new")
	public String newForm (Model model) {		
		model.addAttribute("jspform", new AuthorForm());
		return "admin/author/authoredit";	
	}
	
	@GetMapping("authorselected/{id}")
	public String getSelected (Model model, @PathVariable Integer id) {		
		model.addAttribute("author", authorRepository.findById(id).get());
		return "admin/author/authorselected";		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_AUTHOR_EDIT')")
	@GetMapping("{id}/authoredit")
	public String edit (@PathVariable Integer id, Model model) {
		Author author = authorRepository.findById(id).get();
		AuthorForm authorForm = new AuthorForm();
		authorForm.setId(author.getId());
		authorForm.setSurname(author.getSurname());
		authorForm.setName(author.getName());
		authorForm.setNationality(author.getNationality());
		authorForm.setBirthdate(new SimpleDateFormat("yyyy-MM-dd").format(author.getBirthdate()));
		
		
		model.addAttribute("jspform", authorForm);
		return "admin/author/authoredit";	
	}
			
			
}
