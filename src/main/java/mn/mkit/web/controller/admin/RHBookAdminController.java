package mn.mkit.web.controller.admin;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.AdminBasket;
import mn.mkit.web.domain.BasketItem;
import mn.mkit.web.domain.Book;
import mn.mkit.web.domain.RHBook;
import mn.mkit.web.domain.Reader;
import mn.mkit.web.repository.AdminBasketRepository;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.RHBRepository;
import mn.mkit.web.repository.ReaderRepository;
import mn.mkit.web.repository.WebBasketItemRepository;


@Controller
@RequestMapping(value = "admin/rhbook")
public class RHBookAdminController {

	@Autowired
	private RHBRepository rhbrepository;

	@Autowired
	private AdminBasketRepository basketrepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private ReaderRepository readerRepository;

	@Autowired
	private WebBasketItemRepository wbitemrepo;
	
			
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_RHBOOK_MAIN')")
	@GetMapping
	public String main() {
		return "admin/rhbook";
	}

	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_RHBOOK_LIST')")	
	@GetMapping("rhbooklist")
	public String list(Model model, @RequestParam(required = false) String q, @RequestParam(required = false) Integer readerId, Pageable pageable) {
		if (readerId == null) {
		   model.addAttribute("rhbooks", rhbrepository.searchPage(q, pageable));
		}
		else {
			System.out.println(readerId);
			model.addAttribute("rhbooks", rhbrepository.findByReaderId(readerId, pageable));
		}
			
		return "admin/rhbook/rhbooklist";
	}


	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_RHBOOK_SAVE')")
	@PostMapping("save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(@RequestParam Integer readerId, Principal principal) {

		List<AdminBasket> adminbaskets = basketrepository.findByReaderId(readerId);
		for (AdminBasket item : adminbaskets) {
			RHBook rhbook = new RHBook();
			rhbook.setBook(item.getBook());
			Reader reader = new Reader();
			reader.setId(item.getReaderId());
			rhbook.setReader(reader);
			rhbook.setUsername(principal.getName());
			
			rhbook.setGivendate(LocalDate.now());
			rhbook.setExpireddate(LocalDate.now().plusDays(7)); // sysdate + 7 21+7=28
			rhbook.setStatus("Олгосон");			
			rhbrepository.save(rhbook);
			
			Book book = bookRepository.findById(item.getBook().getId()).get();
			book.setStatus("Олгосон");
			bookRepository.save(book);
			
			basketrepository.deleteById(item.getId());
			
		}
	}

	
	@GetMapping("{id}/rturn")	
	public String rturn(@PathVariable Integer id,Principal principal) {
		
		List<RHBook> rhbooks = rhbrepository.find(id);
		
		if (rhbooks.size() > 0) {
			RHBook rhbook = rhbooks.get(0);					
			rhbook.setUsername(principal.getName());
			rhbook.setStatus("Буцаагдсан");			
			rhbrepository.save(rhbook);
			
			
			Book book = bookRepository.findById(rhbook.getBook().getId()).get();
			book.setStatus("Идэвхтэй");
			bookRepository.save(book);	
		}
		
		
		return "redirect:/admin/rhbook";
	}

	
	@PreAuthorize("hasAnyRole('ROLE_SUPER','ROLE_RHBOOK_LIST')")	
	@GetMapping("webbasketlist")
	public String list(Model model) {
		model.addAttribute("basketitems", wbitemrepo.findByStatus("Захиалсан"));	
		return "admin/rhbook/rhbook1list"; 
	}
	
	
	@GetMapping("webbasket")
	public String webbasket(@RequestParam Integer readerId,@RequestParam Integer basketitemId,Principal principal) {
		
		List<BasketItem> webbasketitems = wbitemrepo.findByWebBasket(basketitemId);
		for (BasketItem item : webbasketitems) {
			RHBook rhbook = new RHBook();
			rhbook.setBook(item.getBook());
			Reader reader = readerRepository.findById(readerId).get();
			rhbook.setReader(reader);
			rhbook.setUsername(principal.getName());
			
			rhbook.setGivendate(LocalDate.now());
			rhbook.setExpireddate(LocalDate.now().plusDays(7)); // sysdate + 7 21+7=28
			rhbook.setStatus("Олгосон");			
			rhbrepository.save(rhbook);
			
			Book book = bookRepository.findById(item.getBook().getId()).get();
			book.setStatus("Олгосон");
			bookRepository.save(book);
			
			wbitemrepo.deleteById(item.getId());
			
		}
		
		return "redirect:/admin/rhbook";
	}
	
	
}
