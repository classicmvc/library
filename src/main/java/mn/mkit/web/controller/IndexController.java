package mn.mkit.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import mn.mkit.web.domain.Authority;
import mn.mkit.web.domain.User;
import mn.mkit.web.repository.AuthorityRepository;
import mn.mkit.web.repository.UserRepository;

@Controller
public class IndexController {
	
	@Autowired
	private UserRepository repo;
	
	@Autowired
	private AuthorityRepository authorityrepository;
		
	//@PreAuthorize("isAuthenticated()") 
	@RequestMapping(value="/")
	public String admin () {		
		return "admin";
	}
		
	
	@RequestMapping(value="login")
	public String login () {	
//		
//		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//		
//		User user = repo.findById("admin").get();
//		user.setPassword(encoder.encode("admin"));
//		repo.save(user);
//		
//		Authority authority = new Authority();
//		authority.setUsername("admin");
//		authority.setAuthority("ROLE_ADMIN");
//		authorityrepository.save(authority);
		
		return "login";
	}
	
	@RequestMapping(value="login-error")
	public String loginError (Model model) {
		model.addAttribute("error", true);
		return "login";
	}
}
