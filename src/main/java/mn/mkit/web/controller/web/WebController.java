package mn.mkit.web.controller.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import mn.mkit.web.domain.Book;
import mn.mkit.web.domain.Category;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.CategoryRepository;

@Controller
public class WebController {
	
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private CategoryRepository categoryRepository;
	
	@RequestMapping(value="/home")
	public String home () {		
		return "home";
	}
	
	@GetMapping("list")
	public String list (Model model, @RequestParam(defaultValue = "0") Integer page) {
		PageRequest request = PageRequest.of(page, 5, Sort.Direction.DESC, "createddate");
		Page<Book> books = bookRepository.findAll(request);
		//Page<Book> books = bookRepository.findByCategory(19, PageRequest.of(page, 5, Sort.Direction.DESC, "createddate"));
		model.addAttribute("books", books);
		return "book/list";		
	}
	
	@GetMapping("/home/category/{categoryId}")
	public String category (Model model, @PathVariable Integer categoryId, @RequestParam(defaultValue = "0") Integer page) {
		Category category = categoryRepository.findById(categoryId).get();
		model.addAttribute("category", category);
		Page<Book> books = bookRepository.findByCategory(categoryId, PageRequest.of(page, 5, Sort.Direction.DESC, "createddate"));	
		model.addAttribute("books", books);
		return "book/list";		
	}
	
	@GetMapping("/home/{id}")
	public String read (Model model, @PathVariable Integer id, HttpSession session) {
		String sessionId = session.getId();  
	    System.out.println("[session-id]: " + sessionId); 	
		Book book = bookRepository.findById(id).get();	
		model.addAttribute("book", book);
		return "book/one";		
	}
	
}
