package mn.mkit.web.controller.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.mkit.web.domain.Basket;
import mn.mkit.web.domain.BasketItem;
import mn.mkit.web.domain.Book;
import mn.mkit.web.repository.BookRepository;
import mn.mkit.web.repository.WebBasketItemRepository;
import mn.mkit.web.repository.WebBasketRepository;

@Controller
@RequestMapping(value = "home")
public class WebBasketController {
	
	@Autowired
	private WebBasketRepository wbrepo;
	
	@Autowired
	private WebBasketItemRepository wbitemrepo;
	
	@Autowired
	private BookRepository bookRepository;
	

	@GetMapping("basket/save")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void save(HttpSession session, @RequestParam Integer bookId) {
		
		
		String sessionId = session.getId();
		Basket wbasket = wbrepo.findSessionId(sessionId);
		Book book = bookRepository.findById(bookId).get();
		BasketItem wbasketitem = new BasketItem();
		
				
		if (wbasket == null) {
			wbasket = new Basket();
			String token;
			token=book.getBarcode()+'-';
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	        Date date = new Date();
	        
	        Integer maxNomer = wbrepo.maxNomerByToday()== null ? 1 : wbrepo.maxNomerByToday();
	        
	        
	        token=token+dateFormat.format(date)+'-'+String.format("%04d", maxNomer);
			wbasket.setToken(token);
			wbasket.setEnterdate(LocalDate.now());
			wbasket.setStatus("Захиалсан");
			wbasket.setNomer(maxNomer);
			wbasket.setSessionId(sessionId);
			wbrepo.save(wbasket);					
		} 
		
		wbasketitem.setBasket(wbasket);
		wbasketitem.setBook(book);
		wbasketitem.setEnterdate(LocalDate.now());
		wbasketitem.setStatus("Захиалсан");
		wbitemrepo.save(wbasketitem);

		book.setStatus("Захиалсан");
		bookRepository.save(book);
			
	} 
	
	
	@PostMapping("basket/confirm")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void confirm(HttpSession session)
	{  
		String sessionId = session.getId();
		Basket bs = wbrepo.findSessionId(sessionId);
		List<BasketItem> basketItems = wbitemrepo.findByBasketId(bs.getId());
		for (BasketItem bi : basketItems) {
			bi.setStatus("Захиалсан");
			wbitemrepo.save(bi);
			System.out.println(sessionId);
		}
		bs.setStatus("Захиалсан");
		wbrepo.save(bs);
	}
	
	@GetMapping("basket/basketlist")
	public String list(HttpSession session, Model model) {
		String sessionId = session.getId();
		Basket wbasket = wbrepo.findSessionId(sessionId);		
		model.addAttribute("basket", wbasket);
		if (wbasket!=null){
			model.addAttribute("basketitems", wbitemrepo.findByBasketId(wbasket.getId()));	
		}	
		return "home/basket/basketlist"; 
	}
	
	@DeleteMapping("basket/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(HttpSession session, @PathVariable Integer id) {
		String sessionId = session.getId();
		BasketItem basketItem = wbitemrepo.findById(id).get();
		wbitemrepo.delete(basketItem);

		Basket wbasket = wbrepo.findSessionId(sessionId);
		
		if (wbitemrepo.findByBasketId(wbasket.getId()).isEmpty()) {
			wbrepo.delete(wbasket);
		}
	}
}
