package mn.mkit.web.modal;


import javax.validation.constraints.NotBlank;

public class CategoryForm {

	private Integer id;
	@NotBlank(message = "Хоосон байж болохгүй")
	private String name;
	private String status;
	private Integer ordering;	
	private Integer parentId;
	
	
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getOrdering() {
		return ordering;
	}
	public void setOrdering(Integer ordering) {
		this.ordering = ordering;
	}
	
	
}
