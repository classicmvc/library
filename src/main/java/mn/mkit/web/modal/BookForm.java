package mn.mkit.web.modal;

import java.time.LocalDate;

import javax.persistence.Column;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class BookForm {

	private Integer id;
	@NotBlank(message = "Хоосон байж болохгүй")
	private String name;
	@Column(unique = true)
	@NotBlank(message = "Хоосон байж болохгүй")
	private String barcode;
	@NotNull(message = "Ангилал заавал сонгоно уу")
	private Integer categoryId;
	private String categoryName;
	@NotNull(message = "Зохиолч заавал сонгоно уу")
	private Integer authorId;
	private String authorName;
	@NotNull(message = "Зураг заавал сонгоно уу")
	private Integer imageId;
	private String description;
	private String status;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate printeddate;
	/*@DateTimeFormat(iso = ISO.DATE)
	private LocalDate createddate;*/
	@NotBlank(message = "Хоосон байж болохгүй")
	private String introduction;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Integer getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}
	
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Integer getImageId() {
		return imageId;
	}
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDate getPrinteddate() {
		return printeddate;
	}
	public void setPrinteddate(LocalDate printeddate) {
		this.printeddate = printeddate;
	}

	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	
	
	
}
