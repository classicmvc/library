package mn.mkit.web.modal;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class RHBookForm {

	private Integer id;
	@NotNull(message = "Номоо заавал сонгоно уу")
	private Integer book_id;
	@NotNull(message = "Уншигч заавал сонгоно уу")
	private Integer reader_id;
	@NotBlank(message = "Хоосон байж болохгүй")
	private String username;
	@DateTimeFormat(iso = ISO.DATE)
	private Date givendate;
	@DateTimeFormat(iso = ISO.DATE)
	private Date expireddate;
	@NotBlank(message = "Хоосон байж болохгүй")
	private String status;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getBook_id() {
		return book_id;
	}
	public void setBook_id(Integer book_id) {
		this.book_id = book_id;
	}
	public Integer getReader_id() {
		return reader_id;
	}
	public void setReader_id(Integer reader_id) {
		this.reader_id = reader_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getGivendate() {
		return givendate;
	}
	public void setGivendate(Date givendate) {
		this.givendate = givendate;
	}
	public Date getExpireddate() {
		return expireddate;
	}
	public void setExpireddate(Date expireddate) {
		this.expireddate = expireddate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
}
