package mn.mkit.web.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "reader_has_book")
public class RHBook {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	private Book book;
	@ManyToOne
	private Reader reader;
	@JoinColumn(name="username")
	private String username;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate givendate;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate expireddate;
	private String status;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	
	
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public LocalDate getGivendate() {
		return givendate;
	}
	public void setGivendate(LocalDate givendate) {
		this.givendate = givendate;
	}
	public LocalDate getExpireddate() {
		return expireddate;
	}
	public void setExpireddate(LocalDate expireddate) {
		this.expireddate = expireddate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
