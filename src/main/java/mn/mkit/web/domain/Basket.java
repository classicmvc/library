package mn.mkit.web.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class Basket {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;
	private String token;
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate enterdate;
	private String status;
	private Integer nomer;
	private String sessionId;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public LocalDate getEnterdate() {
		return enterdate;
	}
	public void setEnterdate(LocalDate enterdate) {
		this.enterdate = enterdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getNomer() {
		return nomer;
	}
	public void setNomer(Integer nomer) {
		this.nomer = nomer;
	}
	
	
	
	
	
	
}
