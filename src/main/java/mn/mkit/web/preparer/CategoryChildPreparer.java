package mn.mkit.web.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.PreparerException;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mn.mkit.web.repository.CategoryRepository;

@Component
public class CategoryChildPreparer {
	
	@Autowired
	private CategoryRepository categoryRepository;

	public void execute(Request tilesRequest, AttributeContext attributeContext) throws PreparerException {	
		Integer parentId = Integer.parseInt(attributeContext.getLocalAttribute("parentId").toString());
		attributeContext.putAttribute("categories", new Attribute(categoryRepository.findByChild(parentId)));
	}

}
