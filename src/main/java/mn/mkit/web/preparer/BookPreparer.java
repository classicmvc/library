package mn.mkit.web.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.PreparerException;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import mn.mkit.web.domain.Book;
import mn.mkit.web.repository.BookRepository;



@Component
public class BookPreparer implements ViewPreparer {
	
	@Autowired BookRepository bookrepository;
	
	public void execute(Request tilesRequest, AttributeContext attributeContext) throws PreparerException {
		Integer size = Integer.parseInt(attributeContext.getLocalAttribute("size").toString()); 
		PageRequest pageRequest = PageRequest.of(0, size, Sort.Direction.DESC, "createddate");
		Page<Book> books = bookrepository.findByAllWeb(pageRequest);
		//Page<Book> books = bookRepository.findByCategory(19, PageRequest.of(page, 5, Sort.Direction.DESC, "createddate"));
		attributeContext.putAttribute("books", new Attribute(books.getContent()));
	}
}


